from filter import Filter, FilterType
import unittest


class TestFilter(unittest.TestCase):
    def test_mean_filter(self):
        f = Filter(FilterType.MEAN, 3)

        input = [1, 4, -6, 0, -3, 100]
        expected_output = [1, 5 / 2, -1 / 3, -2 / 3, -3, 97 / 3]

        self.assertEqual(None, f.get_data())

        for i in range(len(input)):
            f.add(input[i])
            result = f.get_data()
            self.assertEqual(result, expected_output[i])

    def test_posedge_filter(self):
        f = Filter(FilterType.POS_EDGE)

        input = [False, False, True, False, True, True, True, False]
        expected_output = [False, False, True, False, True, False, False, False]

        for i in range(len(input)):
            f.add(input[i])
            result = f.get_data()
            self.assertEqual(result, expected_output[i])

    def test_hold_filter(self):
        f = Filter(FilterType.HOLD, 4)

        input = [True, True, False, True, False, False, False, False, True, True, True, False, True, True, True, True, True, True, False]
        expected_output = [True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, False]

        for i in range(len(input)):
            f.add(input[i])
            result = f.get_data()
            self.assertEqual(result, expected_output[i])

if __name__ == '__main__':
    unittest.main()
