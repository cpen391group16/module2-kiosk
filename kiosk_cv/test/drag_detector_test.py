from drag_detector import DragDetector
import unittest


class TestFilter(unittest.TestCase):
    def test_drag_detector(self):
        d = DragDetector(0.5)

        # This input sequence moves the cursor in a small area, then at least 0.5 units away. The input press is then
        # set to True, and the cursor is then moved at least 0.5 units away. While still holding the press it is brought
        # back to the starting location of the press, where it stays for a few iterations
        input_pos = [(0, 0), (0.1, 0.2), (0.7, 0.1), (1.1, 0.14), (1.1, 0.2),
                     (1.1, 0.21), (1.14, 0.56), (1.2, 0.82), (1.18, 0.81), (1.2, 0.84),
                     (1.13, 0.54), (1.12, 0.43), (1.1, 0.2), (1.1, 0.25), (1.1, 0.25), (1.1, 0.25)]
        input_pressed = [False, False, False, False, False, True, True, True, True, True, True, True, True, True, False,
                         False]
        expected_output = [False, False, False, False, False, False, False, True, True, True, True, True, True, True,
                           False, False]

        for i in range(len(input_pos)):
            result = d.get_drag(input_pos[i], input_pressed[i])
            self.assertEqual(result, expected_output[i])


if __name__ == '__main__':
    unittest.main()
