import unittest

if __name__ == '__main__':
    # Since this file is already in the test folder, search the current folder for the tests
    all_tests = unittest.TestLoader().discover('./', pattern='*_test.py')
    unittest.TextTestRunner().run(all_tests)
