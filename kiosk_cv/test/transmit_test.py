from transmit import Transmit
import unittest


class TestFilter(unittest.TestCase):
    def test_convert_data_to_bytes(self):
        t = Transmit("foo", 0)

        result = t.convert_data_to_bytes(0, 0, False, False, 0)
        expected_result = 0x8000000000.to_bytes(5, 'big', signed=False)
        self.assertEqual(result, expected_result)

        result = t.convert_data_to_bytes(0, 0, True, False, 1)
        expected_result = 0x8500000000.to_bytes(5, 'big', signed=False)
        self.assertEqual(result, expected_result)

        result = t.convert_data_to_bytes(1, 10, False, True, 5)
        expected_result = 0x960001000A.to_bytes(5, 'big', signed=False)
        self.assertEqual(result, expected_result)

        result = t.convert_data_to_bytes(589, 204, True, True, 4)
        expected_result = 0x92044D014C.to_bytes(5, 'big', signed=False)
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
