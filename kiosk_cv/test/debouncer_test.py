from debouncer import Debouncer
import unittest


class TestFilter(unittest.TestCase):
    def test_debouncer(self):
        d = Debouncer(3)

        input = [True, True, False, True, False, False, True, True, False, False, False, False, False, True, False]
        expected_output = [True, True, True, True, True, True, True, True, True, True, False, False, False, False,
                           False]

        for i in range(len(input)):
            result = d.get_data(input[i])
            self.assertEqual(result, expected_output[i])


if __name__ == '__main__':
    unittest.main()
