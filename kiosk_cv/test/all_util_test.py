import util
import math
import unittest


class TestUtil(unittest.TestCase):
    def test_tuple_in_list(self):
        data = [(0, 0), (1, 1), (3, 5), (-1, -2), (0, -3)]

        result = util.tuple_in_list(data, data[0], 0.5)
        self.assertFalse(result)

        result = util.tuple_in_list(data, data[0], 1)
        self.assertFalse(result)

        result = util.tuple_in_list(data, data[0], math.sqrt(2))
        self.assertTrue(result)

        result = util.tuple_in_list(data, data[4], 1)
        self.assertFalse(result)

        result = util.tuple_in_list(data, data[4], 5)
        self.assertTrue(result)

    def test_average_points(self):
        data = [(0, 0), (1, 1), (3, 5), (-1, -2), (0, -3)]

        result = util.average_points(data[0:1])
        self.assertEqual(result, (0, 0))

        result = util.average_points(data[0:3])
        self.assertEqual(result, (float(4 / 3), 2))

        result = util.average_points(data[-2:])
        self.assertEqual(result, (-0.5, -2.5))

        result = util.average_points(data)
        self.assertEqual(result, (0.6, 0.2))

    # For nested list comparison: https://stackoverflow.com/questions/6105777/how-to-compare-a-list-of-lists-sets-in-python
    def test_group_points_within_threshold(self):
        data = [(0, 0), (0, 1), (1, 1), (3, 5), (-1, -2), (0, -3)]

        result = util.group_points_within_threshold(data, 0)
        expected_result = [[x] for x in data]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 1)
        expected_result = [[(0, 0), (0, 1), (1, 1)], [(3, 5)], [(-1, -2)], [(0, -3)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 2)
        expected_result = [[(0, 0), (0, 1), (1, 1)], [(3, 5)], [(-1, -2), (0, -3)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 3)
        expected_result = [[(0, 0), (0, 1), (1, 1), (-1, -2), (0, -3)], [(3, 5)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 4)
        expected_result = [[(0, 0), (0, 1), (1, 1), (-1, -2), (0, -3)], [(3, 5)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 5)
        expected_result = [[(0, 0), (0, 1), (1, 1), (3, 5), (-1, -2), (0, -3)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.group_points_within_threshold(data, 6)
        expected_result = [[(0, 0), (0, 1), (1, 1), (3, 5), (-1, -2), (0, -3)]]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

    def test_combine_points_within_threshold(self):
        data = [(0, 0), (0, 1), (1, 1), (3, 5), (-1, -2), (0, -3)]

        result = util.combine_points_within_thresh(data, 0)
        expected_result = [(0, 0), (0, 1), (1, 1), (3, 5), (-1, -2), (0, -3)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.combine_points_within_thresh(data, 1)
        expected_result = [(1 / 3, 2 / 3), (3, 5), (-1, -2), (0, -3)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.combine_points_within_thresh(data, 2)
        expected_result = [(1 / 3, 2 / 3), (3, 5), (-1 / 2, -5 / 2)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.combine_points_within_thresh(data, 3)
        expected_result = [(0, -3 / 5), (3, 5)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.combine_points_within_thresh(data, 4)
        expected_result = [(0, -3 / 5), (3, 5)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)

        result = util.combine_points_within_thresh(data, 5)
        expected_result = [(3 / 6, 2 / 6)]
        result = set(map(tuple, result))
        expected_result = set(map(tuple, expected_result))
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
