import math


class DragDetector:
    """
    This class is responsible for detecting when the cursor/tracking object is initiating a drag movement
    """

    def __init__(self, threshold):
        """
        Initializes the DragDetector
        :param threshold: The threshold for how much the cursor must move while clicking before it is considered a drag.
        This is a normalized value between 0 and 1, representing the fraction of the screen the cursor has been moved
        """
        self.threshold = threshold
        self.last_click_pos = None
        self.drag = False

    def get_drag(self, cursor_pos, pressed):
        """
        Given a new click position, detects whether or not a drag is taking place
        :param cursor_pos: The position of the cursor, given in normalized coordinates between o and 1
        :param click: A boolean representing whether or not the cursor is pressed
        :return: True if a drag is detected, and False otherwise
        """
        if self.last_click_pos is None and pressed:
            self.last_click_pos = cursor_pos
            self.drag = False
        elif not pressed:
            self.last_click_pos = None
            self.drag = False
        elif math.sqrt((cursor_pos[0] - self.last_click_pos[0]) ** 2 +
                                       (cursor_pos[1] - self.last_click_pos[1]) ** 2) >= self.threshold:
            self.drag = True

        return self.drag
