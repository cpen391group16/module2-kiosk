import cv2
import numpy as np
import util


class Camera:
    """
    A class that wraps and represents a camera
    """

    def __init__(self, capture_index, name):
        """
        Creates a new Camera object, with the given capture index and name
        :param capture_index: The opencv video capture index for this camera
        :param name: The name for this camera
        """
        self.name = name

        # Values for the red object HSV filter
        self.brl = np.array([172, 50, 50])
        self.bru = np.array([199, 255, 255])
        self.drl = np.array([0, 70, 70])
        self.dru = np.array([5, 255, 255])

        try:
            self.cap = cv2.VideoCapture(capture_index)
            self.width = int(self.cap.get(3))
            self.height = int(self.cap.get(4))
        except Exception:
            print("Error: Camera {} was not opened successfully".format(name))
            self.cap = None

        # Values used for camshift object tracking
        # These are member variables because each frame needs to keep track of its own tracking window and values
        # for histeresis
        self.track_window = None
        self.dst = None
        self.term_crit = None
        self.roi_hist = None

    def __del__(self):
        """
        A custom destructor for the Camera. Releases the internal capture object if it was being held
        """
        if self.is_valid():
            self.cap.release()

    def is_valid(self):
        """
        Returns True if the Camera object was created successfully, and False otherwise. This function is provided to
        help modules that use the Camera to remain tolerant to failures in camera creation and decide what they want
        to do when Camera creation fails.
        :return: True if the Camera object was created successfully, and False otherwise
        """
        return self.cap is not None and self.cap.isOpened()

    def get_frame(self):
        """
        Returns a frame from this camera
        :return: A single frame from this camera in the BGR colorspace
        """
        if self.is_valid():
            ret, frame = self.cap.read()
            if ret:
                return frame
        return None

    def init_ball_camshift(self, frame):
        """
        Initializes member variables for camshift tracking of a red ball
        :param frame: A frame from this camera in the BGR colorspace
        """
        # Set up the tracking window. We can use arbitrary values since this is only a starting point for the window.
        # The algorithm will move it to the correct place
        r, c, h, w = 250, 90, 400, 125
        self.track_window = (c, r, w, h)

        # Set up the Range Of Intereset for tracking
        roi = frame[r:r + h, c:c + w]
        hsv_roi = roi

        # Created a combined mask for the red ball from bright and dark red
        mask_1 = cv2.inRange(hsv_roi, self.brl, self.bru)
        mask_2 = cv2.inRange(hsv_roi, self.drl, self.dru)
        mask = cv2.addWeighted(mask_1, 1.0, mask_2, 1.0, 0.0)

        self.roi_hist = cv2.calcHist([hsv_roi], [0], mask, [180], [0, 180])
        cv2.normalize(self.roi_hist, self.roi_hist, 0, 255, cv2.NORM_MINMAX, 1)

        # Setup the termination criteria, either 15 iterations or move by at least 1 pt
        self.term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 15, 1)

    def get_ball_camshift_pos(self, frame):
        """
        Takes a frame in the BGR colorspace and returns the position of the tracking object using camshift
        :param frame: A frame in the BGR colorspace
        :return: The position (x, y) of the tracking object, where x, y are normalized between [0-1]
        """
        hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv_frame], [0], self.roi_hist, [0, 180], 1)

        ret, self.track_window = cv2.CamShift(dst, self.track_window, self.term_crit)

        # Draw it on image
        x, y, w, h = self.track_window
        camshift_frame = cv2.rectangle(frame, (x, y), (x + w, y + h), 255, 2)

        cv2.imshow(self.name + ' camshift', camshift_frame)

        # Return the center of the tracking window
        pos = (x + w / 2, y + h / 2)
        pos = (pos[0] / self.width, pos[1] / self.height)
        return pos

    def get_ball_contour_pos(self, frame):
        return util.get_red_object_contour_position(frame, self.width, self.height, self.name)

    def get_hand_contour(self, frame):
        """
        Given a frame in the BGR colorspace, returns the contour of the hand if one is detected, and None otherwise
        :param frame: A frame in the BGR colorspace
        :return: The contour for the hand if it can be found, and None otherwise
        """
        # Read tuning parameters from the trackbars
        h_step = cv2.getTrackbarPos('H Step', 'Hand Settings')
        h_range = cv2.getTrackbarPos('H Range', 'Hand Settings')
        h_low = cv2.getTrackbarPos('H Discard Range Lower', 'Hand Settings')
        h_high = cv2.getTrackbarPos('H Discard Range Upper', 'Hand Settings')
        min_score = cv2.getTrackbarPos('Hand Min Score', 'Hand Settings')

        filtered_frames = util.get_hsv_filter_frames(frame, h_step, h_range, h_low, h_high)

        for i in range(len(filtered_frames)):
            cv2.imshow("Frame {}".format(i), filtered_frames[i])

        # Create all possible combinations of 3 filtered frames. This means that if 2 frames each detected a different
        # part of the hand, we will combine them to make a better mask
        combined_filtered_frames = []
        for i in range(len(filtered_frames)):
            for j in range(len(filtered_frames)):
                for k in range(len(filtered_frames)):
                    combined_frame = cv2.addWeighted(filtered_frames[i], 1, filtered_frames[j], 1, 0)
                    combined_frame = cv2.addWeighted(combined_frame, 1, filtered_frames[k], 1, 0)
                    combined_filtered_frames.append(combined_frame)

        filtered_frames = combined_filtered_frames

        # Get the largest contour from each frame
        contours = util.get_n_largest_contours(filtered_frames, len(filtered_frames))

        # Evaluate the contours to find the one that most likely represents a hand
        best_score = -1
        hand_contour = None
        for c in contours:
            score = util.get_hand_confidence_score(c)

            if score > best_score:
                best_score = score
                hand_contour = c

        # Return None if we don't think there is a hand in the frame
        if hand_contour is None or best_score < min_score:
            return None

        return hand_contour

    def normalize_pos(self, pos, edge_threshold):
        """
        Given a position, returns the normalized position with coordinates x, y between 0-1
        Eg. 0.5, 0.5 corresponds to a position in the centre of the frame
        :param pos: The position to normalize
        :param edge_threshold: A value between 0-1 of how far inwards from the edge of the frame is considered the "max". For example, a value
        of 0.1 will cause the normalized x coordinate to be 1.0 when the given x coordinate is 90% or more of the width
        of the camera's frames
        :return: The position with coordinates normalized between 0-1 based on the dimensions of the camera, and the
        corners of the rectangle representing the thresholded area of the frame
        """
        x_min = self.width * edge_threshold
        x_max = self.width - x_min
        y_min = self.height * edge_threshold
        y_max = self.height - y_min

        norm_x = (pos[0] - x_min) / float(x_max - x_min)
        norm_x = min(norm_x, 1.0)
        norm_x = max(norm_x, 0.0)

        norm_y = (pos[1] - y_min) / float(y_max - y_min)
        norm_y = min(norm_y, 1.0)
        norm_y = max(norm_y, 0.0)

        top_left = (int(x_min), int(y_min))
        bottom_right = (int(x_max), int(y_max))

        return (norm_x, norm_y), top_left, bottom_right
