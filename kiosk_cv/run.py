import cv2
import numpy as np
from datetime import datetime
import util
from camera import Camera
from transmit import Transmit
from filter import Filter, FilterType
from debouncer import Debouncer
from drag_detector import DragDetector

# Resources used, mostly opencv tutorials
# Basic videos - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html#display-video
# Colors and SImple object tracking - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html#converting-colorspaces
# Contours - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_contours/py_contours_begin/py_contours_begin.html#contours-getting-started
# Contours - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_contours/py_contour_features/py_contour_features.html#contour-features
# contours - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_contours/py_contour_properties/py_contour_properties.html#contour-properties
# Camshift - https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_video/py_meanshift/py_meanshift.html#meanshift


# The width and height of the kiosk display
DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 480

util.create_hand_trackbars()

# Create the camera objects
cam_top = Camera(0, "Top")
cam_side = Camera(1, "Side")

# GIve initial values in case cameras do not get created successfully
top_fps = 1
side_fps = 1

# If the camera are valid, calculate their fps and initialize campshift
if cam_top.is_valid():
    top_fps = util.calc_fps(cam_top)
    cam_top.init_ball_camshift(cam_top.get_frame())

if cam_side.is_valid():
    side_fps = util.calc_fps(cam_side)
    cam_side.init_ball_camshift(cam_side.get_frame())

# Create filters for the coordinates of the object
x_filter = Filter(FilterType.MEAN, 5)
y_filter = Filter(FilterType.MEAN, 5)
z_filter = Filter(FilterType.MEAN, 5)

# Instantiate modules to filter and debounce cursor data
press_debouncer = Debouncer(3)
click_filter = Filter(FilterType.POS_EDGE)
drag_detector = DragDetector(0.025)
num_fingers_debouncer = Debouncer(3)

# Create a module to detec when the cursor is being held. Calculate the size/number of frames to instantiate it with
# based on the FPS of the camera
HOLD_DELAY_SECONDS = 0.5
hold_detector = Filter(FilterType.HOLD, int(HOLD_DELAY_SECONDS * top_fps))

# Create a transmitter for Transmitting to the De1-Soc (Kiosk)
# For linux
# de1_transmit = Transmit('/dev/ttyUSB0', 9600)
# For windows
de1_transmit = Transmit('COM5', 9600)

SERIAL_TRANSMISSIONS_PER_SECOND = 30

# Timer variables used to control the rate at which we send serial data
t1 = datetime.now()
t2 = t1

# Main loop
while True:
    ####################################################################################################################
    # Initialize Variables
    ####################################################################################################################
    top_frame = None
    top_contour = None

    ####################################################################################################################
    # Read camera data
    ####################################################################################################################
    if cam_top.is_valid():
        top_frame = cam_top.get_frame()

        if top_frame is not None:
            top_contour = cam_top.get_hand_contour(top_frame)

    if top_contour is not None:
        # Turn the hand contour back into an image mask, and use that to calculate the position of the hand
        hand_frame = np.zeros((cam_top.height, cam_top.width, 1), np.uint8)
        cv2.drawContours(hand_frame, [top_contour], -1, 255, -1)
        cv2.imshow("Combined Frame", hand_frame)
        top_pos = util.get_center_of_hand_frame(hand_frame)
        top_pos_2 = util.get_center_of_contour(top_contour)

        # If the center returned by the frame is None, we can fall back on the center of the original contour
        top_pos = top_pos if top_pos is not None else top_pos_2
    else:
        top_pos = None

    if top_pos is not None:
        ################################################################################################################
        # Get cursor position
        ################################################################################################################
        normalized_top_pos, top_left, bot_right = cam_top.normalize_pos(top_pos, 0.15)

        # Draw the area in which the cursor can be controlled
        cv2.rectangle(top_frame, top_left, bot_right, [255, 255, 255], 4)

        x_filter.add(normalized_top_pos[0])
        y_filter.add(normalized_top_pos[1])

        # Get the filtered data for the coordinates
        normalized_x = x_filter.get_data()
        normalized_y = y_filter.get_data()

        normalized_cursor_pos = (1 - normalized_x, 1 - normalized_y)
        cursor_pos = (int(normalized_cursor_pos[0] * DISPLAY_WIDTH), int(normalized_cursor_pos[1] * DISPLAY_HEIGHT))

        ################################################################################################################
        # Calculate Hand Attributes
        ################################################################################################################
        fingers = util.get_hand_fingers(top_contour, top_pos)
        num_fingers = num_fingers_debouncer.get_data(len(fingers))

        util.draw_hand(top_frame, top_contour, fingers, top_pos)

        ################################################################################################################
        # Calculate cursor attributes
        ################################################################################################################
        cursor_pressed = util.is_hand_closed(fingers, top_pos)
        cursor_pressed = press_debouncer.get_data(cursor_pressed)

        click_filter.add(cursor_pressed)
        cursor_clicked = click_filter.get_data()

        cursor_dragged = drag_detector.get_drag(normalized_cursor_pos, cursor_pressed)

        cursor_held = hold_detector.add(cursor_pressed)
        cursor_held = hold_detector.get_data()

        # If the user hold down the cursor in the same spot for a while, we will assume they are going to drag
        cursor_dragged = cursor_dragged or cursor_held
    else:
        # If we don't have a valid cursor position, we send a position that is off the screen so that it will not be drawn
        cursor_pos = (int(1.5 * DISPLAY_WIDTH), int(1.5 * DISPLAY_HEIGHT))
        cursor_pressed = False
        cursor_dragged = False
        num_fingers = 0

    ################################################################################################################
    # Send Data
    ################################################################################################################
    t2 = datetime.now()
    if (t2 - t1).total_seconds() * 1000 > int(1000 / SERIAL_TRANSMISSIONS_PER_SECOND):
        t1 = t2

        print("({}, {}), {}, {}, {}".format(cursor_pos[0], cursor_pos[1], cursor_pressed, cursor_dragged,
                                            num_fingers))

        if de1_transmit.is_valid():
            de1_transmit.send_cursor_data(cursor_pos[0], cursor_pos[1], cursor_pressed, cursor_dragged, num_fingers)

    ####################################################################################################################
    # Draw Frame
    ####################################################################################################################
    if top_frame is not None:
        # Flip the frame so it is drawn with the correct perspective from the POV of the user
        cv2.imshow("Top", cv2.flip(top_frame, -1))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
