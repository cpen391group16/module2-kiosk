from enum import Enum
# We use a deque here rather than queue because it's iterable, and therefore gives us access to functions like 'sum'
from collections import deque


class FilterType(Enum):
    """
    A simple enum class, used in combination with the Filter class to specify the type of filter to be created
    """
    MEAN = 1
    POS_EDGE = 2
    HOLD = 3


class Filter:
    """
    A filter for various forms of data. The type of filter can be specified with the FilterType enum
    as an input parameter upon creation.
    """

    def __init__(self, filter_type, size=2):
        """
        Create a new Filter object
        :param filter_type: An enum of type FilterType that specifies the type of filter. MEAN creates a mean filter (averages
        values), POS_EDGE creates a filter that detects the "positive edge" of boolean data, and HOLD created a filter that
        only returns true when all it's data is true
        :param size: The size of the filter. This specifies how many entries are stored in the Filter's state and used to calculate results
        """
        self.type = filter_type
        # Use this deque as a normal queue. Insert from the right, pop from the left
        # We use a deque because it is iterable and gives the ability to use functions like "sum"
        self.data = deque(maxlen=size)

    def add(self, data):
        """
        If the new data is not None, add it to the internal array. Make sure the array length does not exceed
        the size of the filter
        :param data: The data to add to the filter. If None is given, it is ignored.
        """
        if data is not None:
            # Since this is a deque, if the deque is full data will automatically be popped from the other side
            # so we don't need cases for when the deque is full or not
            self.data.append(data)

    # TODO: Test hold mode
    def get_data(self):
        """
        Returns the filtered data of this filter. Data returned depends on the type of filter created
        :return: The filtered data from this filter
        """
        # Returns the mean of the filter's data
        if self.type == FilterType.MEAN:
            if self.data:
                return sum(self.data) / float(len(self.data))
        # Returns True if a posedge is detected in the filter's data, and False otherwise
        elif self.type == FilterType.POS_EDGE:
            if len(self.data) >= 2:
                data_list = list(self.data)
                return not data_list[0] and data_list[1]
            else:
                return False
        # Returns True of all the filter's data is True, and False otherwise
        elif self.type == FilterType.HOLD:
            all_data_true = all(x is True for x in self.data)
            return all_data_true

        return None
