import serial


class Transmit:
    """
    This class is an abstraction for transmitting data to the De1 and Nios system.
    An is_valid check is provided so that systems using this module can be robust to failures in creating
    the serial connection, and can avoid calls to an invalid serial connection
    """

    def __init__(self, port, baudrate, bytesize=8, parity='N', stopbits=1, timeout=None):
        """
        Create a new Transmit object
        :param port: The name of the port/device to connect with over serial
        :param baudrate: The baudrate for the serial connectino
        """
        self.port_name = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout

        try:
            self.ser = serial.Serial(port=self.port_name, baudrate=self.baudrate, bytesize=self.bytesize,
                                     parity=self.parity, stopbits=self.stopbits, timeout=self.timeout)

            # Flush the output to make sure we have a clear buffer when we start
            self.ser.flushOutput()
            self.valid = True
        except Exception:
            print("Error: Unable to create serial connection to {}".format(port))
            self.valid = False

    def __del__(self):
        """
        The class destructor. Make sure to release the serial object.
        """
        if self.is_valid():
            self.ser.close()

    def is_valid(self):
        """
        Returns the status of the Transmitter object (whether or not it's valid)
        :return: True if the Transmitter object is valid (was created successfully), and False otherwise
        """
        return self.valid

    def convert_data_to_bytes(self, x, y, is_pressed, is_dragged, num_fingers):
        """
        Given data for the cursor, converts the data into byte data (binary) to be sent over serial.
        :param x: A  x coordinate
        :param y: A y coordinate
        :param is_pressed Whether or not the cursor is being pressed
        :param is_dragged: Whether or not the cursor is currently being dragged
        :param num_fingers: How many fingers are extended
        :return: A byte object containing the given data
        """
        # Because of how we read signed data on the Nios, we shift our bytes so that the most significant
        # bit is not one (and still send the byte as unsigned). This allows the header byte to be the only byte
        # with a leading one, and be detected by the Nios decoder.
        x_data_upper = (x >> 7).to_bytes(1, 'big', signed=False)
        x_data_lower = (x & 0x7F).to_bytes(1, 'big', signed=False)
        y_data_upper = (y >> 7).to_bytes(1, 'big', signed=False)
        y_data_lower = (y & 0x7F).to_bytes(1, 'big', signed=False)

        # Header byte encoding
        # MSB
        # 1     1 indicates header byte (all other bytes start with 0)
        # x     # unused
        # x     # unused
        # F1    #
        # F2    #
        # F3    # These 3 bits are used to pass the number of extended fingers. F1 is the MSB, F3 is the LSB
        # 0     1 if cursor is being dragged, 0 else
        # 0     1 if cursor is pressed0, else
        # LSB
        #
        header = 0x80
        if is_pressed:
            header = 0x81
        if is_dragged:
            header = 0x82
        header = header | num_fingers << 2

        header = header.to_bytes(1, 'big')

        message = header + \
                  x_data_upper + \
                  x_data_lower + \
                  y_data_upper + \
                  y_data_lower

        return message

    def send_cursor_data(self, x, y, is_pressed, is_dragged, num_fingers):
        """
        Sends the given data to the serial connection
        :param x: The x coordinate of the cursor
        :param y: The y coordinate of the cursor
        :param is_pressed: Whether or not the cursor is being pressed
        :param is_dragged: Whether or not the cursor is being dragged
        """
        message = self.convert_data_to_bytes(x, y, is_pressed, is_dragged, num_fingers)
        self.ser.write(message)
