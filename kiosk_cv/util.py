import cv2
import numpy as np
import math
import time


def nothing(x):
    """
    An empty utility function that does nothing
    """
    pass


def create_ball_trackbars():
    """
    Creates trackbar objects for testing and tuning various parameters
    :return: None
    """
    cv2.namedWindow('Ball Settings')
    cv2.createTrackbar('HSV Blur', 'Ball Settings', 18, 50, nothing)
    cv2.createTrackbar('Erode', 'Ball Settings', 8, 30, nothing)
    cv2.createTrackbar('Dilate', 'Ball Settings', 21, 30, nothing)

    cv2.createTrackbar('Bright Red Lower H', 'Ball Settings', 172, 255, nothing)
    cv2.createTrackbar('Bright Red Lower S', 'Ball Settings', 49, 255, nothing)
    cv2.createTrackbar('Bright Red Lower V', 'Ball Settings', 49, 255, nothing)
    cv2.createTrackbar('Bright Red Upper H', 'Ball Settings', 199, 255, nothing)
    cv2.createTrackbar('Bright Red Upper S', 'Ball Settings', 255, 255, nothing)
    cv2.createTrackbar('Bright Red Upper V', 'Ball Settings', 255, 255, nothing)

    cv2.createTrackbar('Dark Red Lower H', 'Ball Settings', 0, 255, nothing)
    cv2.createTrackbar('Dark Red Lower S', 'Ball Settings', 70, 255, nothing)
    cv2.createTrackbar('Dark Red Lower V', 'Ball Settings', 70, 255, nothing)
    cv2.createTrackbar('Dark Red Upper H', 'Ball Settings', 5, 255, nothing)
    cv2.createTrackbar('Dark Red Upper S', 'Ball Settings', 255, 255, nothing)
    cv2.createTrackbar('Dark Red Upper V', 'Ball Settings', 255, 255, nothing)


def get_red_object_contour_position(frame, width, height, display=False, prefix="Red Contour"):
    """
    Takes an opencv frame, and returns a tuple (x, y), where x and y are doubles in the range
    [0,1] representing the normalized location of the tracking object in the frame
    eg. (0.5, 0.5) means the object is in the middle of the frame
    :param frame: An opencv frame in the BGR colorspace
    :param width: The width of the frame in pixels
    :param height: The height of the frame in pixels
    :param display: Specifies whether or not calculations and partial results should be displayed
    :param prefix: The prefix string to append to any displays
    :return: A tuple (x, y) representing the normalized position of the tracking object in the frame if the tracking
            object can be found, and None otherwise
    """

    # Read values from sliders
    blur = max(cv2.getTrackbarPos('HSV Blur', 'Ball Settings'), 1)
    erode = max(cv2.getTrackbarPos('Erode', 'Ball Settings'), 1)
    dilate = max(cv2.getTrackbarPos('Dilate', 'Ball Settings'), 1)

    brlh = cv2.getTrackbarPos('Bright Red Lower H', 'Ball Settings')
    brls = cv2.getTrackbarPos('Bright Red Lower S', 'Ball Settings')
    brlv = cv2.getTrackbarPos('Bright Red Lower V', 'Ball Settings')
    bruh = cv2.getTrackbarPos('Bright Red Upper H', 'Ball Settings')
    brus = cv2.getTrackbarPos('Bright Red Upper S', 'Ball Settings')
    bruv = cv2.getTrackbarPos('Bright Red Upper V', 'Ball Settings')

    drlh = cv2.getTrackbarPos('Dark Red Lower H', 'Ball Settings')
    drls = cv2.getTrackbarPos('Dark Red Lower S', 'Ball Settings')
    drlv = cv2.getTrackbarPos('Dark Red Lower V', 'Ball Settings')
    druh = cv2.getTrackbarPos('Dark Red Upper H', 'Ball Settings')
    drus = cv2.getTrackbarPos('Dark Red Upper S', 'Ball Settings')
    druv = cv2.getTrackbarPos('Dark Red Upper V', 'Ball Settings')

    # Converting the input stream into HSV color space
    # This makes color grouping and detection easier
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # blur the frame to remove harsh transitions
    frame_hsv_blur = cv2.blur(frame_hsv, (blur, blur))

    # Define the range of Red we want to use as the threshold for finding the red ball
    bright_red_lower_bound = (brlh, brls, brlv)
    bright_red_upper_bound = (bruh, brus, bruv)

    # Define the range of Red we want to use as the threshold for finding the red ball.
    # Because we want to ignore human flesh and other "middle"
    dark_red_lower_bound = (drlh, drls, drlv)
    dark_red_upper_bound = (druh, drus, druv)

    # The frame mask for the red ball
    bright_red_mask = cv2.inRange(frame_hsv_blur, bright_red_lower_bound, bright_red_upper_bound)
    dark_red_mask = cv2.inRange(frame_hsv_blur, dark_red_lower_bound, dark_red_upper_bound)

    if display:
        cv2.imshow(prefix + " Dark Red Mask", dark_red_mask)
        cv2.imshow(prefix + " Bright Red Mask", bright_red_mask)

    # Combine the dark and light red masks to get a total mask for the red object
    weighted_mask = cv2.addWeighted(bright_red_mask, 1.0, dark_red_mask, 1.0, 0.0)

    # Blur the mask to smooth it out and remove tiny discrepancies
    blurred_mask = cv2.GaussianBlur(weighted_mask, (9, 9), 3, 3)

    # Erode and then dilate the mask to remove small blobs. Eroding removes them and dilating repairs the
    # larger part of the mask we want (assuming this is the ball)
    erode_element = cv2.getStructuringElement(cv2.MORPH_RECT, (erode, erode))
    dilate_element = cv2.getStructuringElement(cv2.MORPH_RECT, (dilate, dilate))
    eroded_mask = cv2.erode(blurred_mask, erode_element)
    dilated_mask = cv2.dilate(eroded_mask, dilate_element)

    if display:
        cv2.imshow(prefix + ' dilated mask', dilated_mask)

    # Threshold the result to get a binary image to find contours
    ret, thresh = cv2.threshold(dilated_mask, 127, 255, 0)
    _, contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) != 0:
        # Find the contour with the biggest area. Because we have filtered by color we assume this is the object we
        # want to track
        c = max(contours, key=cv2.contourArea)

        x, y, w, h = cv2.boundingRect(c)

        # Draw the contour of the largest contour
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        if display:
            cv2.imshow(prefix + " contour", frame)

        # Find the center of the bounding rectangle, and normalize this position within the frame before returning it
        pos = (x + w / 2, y + w / 2)
        pos = (pos[0] / width, pos[1] / height)
        return pos

    return None


def create_hand_trackbars():
    """
    Creates trackbars to tune and modify values for hand detection
    """
    cv2.namedWindow('Hand Settings')
    cv2.createTrackbar('H Step', 'Hand Settings', 5, 100, nothing)
    cv2.createTrackbar('H Range', 'Hand Settings', 10, 100, nothing)
    cv2.createTrackbar('H Discard Range Lower', 'Hand Settings', 17, 255, nothing)
    cv2.createTrackbar('H Discard Range Upper', 'Hand Settings', 255, 255, nothing)
    cv2.createTrackbar('Hand Max Defects', 'Hand Settings', 35, 50, nothing)
    cv2.createTrackbar('Hand Min Score', 'Hand Settings', 4000, 30000, nothing)
    cv2.createTrackbar("Defect Dist Ratio", "Hand Settings", 1700, 3000, nothing)
    cv2.createTrackbar("Erode size", "Hand Settings", 2, 30, nothing)
    cv2.createTrackbar("Palm Erode size", "Hand Settings", 45, 50, nothing)
    cv2.createTrackbar("Dilate size", "Hand Settings", 2, 30, nothing)
    cv2.createTrackbar("Frame blur size", "Hand Settings", 5, 30, nothing)
    cv2.createTrackbar("Mask blur size", "Hand Settings", 5, 30, nothing)
    cv2.createTrackbar("BG Thresh", "Hand Settings", 50, 255, nothing)


def get_hsv_filter_frames(frame, h_step, h_range, h_discard_range_low, h_discard_range_high):
    """
    Filters the frame by hsv values, and returns a frame for each filter. The filter is performed across the entire hsv
    colorspace, with hsv values starting from 0 and going to 255, with increments of hsv_step. Eg if hsv_step is 5, this
    function will return a frame with H values filtered from 0-5, 5-10, 10-15, etc. Any values within the discard range
    are ignored.
    :param frame: A frame in the BGR colorspace
    :param h_step: The increment for h values to be filtered
    :param h_range: The size of each filter frame (the range of the h value for each filter)
    :param h_discard_range_low: The h value above which to ignore (up to h_discard_range_high)
    :param h_discard_range_high: the h value below which to ignore (down to h_discard_range_low)
    :return: A list of frames with hsv filters applied
    """
    assert (h_step > 0)
    assert (h_range > 0)

    erode_size = max(cv2.getTrackbarPos("Erode size", "Hand Settings"), 1)
    dilate_size = max(cv2.getTrackbarPos("Dilate size", "Hand Settings"), 1)

    # Because the blur kernels must have an odd size (so there is a definite middle pixel), we check here to make sure
    # the parameters are odd, and adjust them if necessary
    frame_blur_size = max(cv2.getTrackbarPos("Frame blur size", "Hand Settings"), 1)
    frame_blur_size = frame_blur_size + 1 if frame_blur_size % 2 == 0 else frame_blur_size
    mask_blur_size = max(cv2.getTrackbarPos("Mask blur size", "Hand Settings"), 1)
    mask_blur_size = 1 if mask_blur_size % 2 == 0 else mask_blur_size

    # Converting the input frame into the HSV color space
    # This makes color grouping and detection easier
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Blur the frame to remove harsh transitions
    hsv_blur = cv2.GaussianBlur(hsv, (frame_blur_size, frame_blur_size), 1, 1)

    # Below we step through the HSV colorspace to append frames/masks to the list of filtered frames
    # We do not consider any filters in the "discard range" of the colorspace. This allows to us speed up calculations
    # by selecting roughly what range of the colorspace we care about.
    filtered_frames = []

    # Add filters for the lower range of h
    h = 0
    while h < h_discard_range_low:
        lower = np.array([h, 50, 50])
        upper = np.array([min(h + h_range, h_discard_range_low), 255, 255])
        filtered_frames.append(cv2.inRange(hsv_blur, lower, upper))
        h = h + h_step

    h = h_discard_range_high
    while h < 255:
        lower = np.array([h, 0, 0])
        upper = np.array([min(h + h_range, 255), 255, 255])
        filtered_frames.append(cv2.inRange(hsv_blur, lower, upper))
        h = h + h_step

    # Perform postprocessing on each filtered frame, applying blur and erode/dilate to correct small blobs
    for i in range(len(filtered_frames)):
        f = filtered_frames[i]

        # Blur all the hsv filtered frames to remove noise from the masks
        blur = cv2.GaussianBlur(f, (5, 5), 1, 1)
        blur = cv2.medianBlur(blur, mask_blur_size)

        # Erode and then dilate the mask to remove small blobs. Eroding removes them and dilating repairs the
        # larger part of the mask we want (assuming this is the ball)
        erode_element = cv2.getStructuringElement(cv2.MORPH_RECT, (erode_size, erode_size))
        dilate_element = cv2.getStructuringElement(cv2.MORPH_RECT, (dilate_size, dilate_size))
        eroded_mask = cv2.erode(blur, erode_element)
        dilated_mask = cv2.dilate(eroded_mask, dilate_element)

        filtered_frames[i] = dilated_mask

    return filtered_frames


def get_hand_confidence_score(contour):
    """
    An evaluation function that returns a positive confidence score for whether or not a given contour represents a hand.
    Contours that are more likely to be a hand have a higher score
    :param contour: The contour to evaluate. Must not be None
    :return: A score representing the confidence that this contour is a hand
    """
    # The max convex hull defects a hand contour is allowed to have, without suffering large score penalties. This
    # helps filter very convoluted contours that are not likely to be a hand
    max_defects = cv2.getTrackbarPos('Hand Max Defects', 'Settings')

    area = cv2.contourArea(contour)
    perimeter = cv2.arcLength(contour, True)
    hull = cv2.convexHull(contour, returnPoints=False)
    hull_points = cv2.convexHull(contour, returnPoints=True)

    # We need to calculate the hull with return points here to get the area
    hull_area = cv2.contourArea(hull_points)

    # Solidity is the ratio of a contour area to its convex hull area
    solidity = float(area) / hull_area if hull_area != 0 else math.inf

    hull_perimeter = cv2.arcLength(hull_points, True)
    defects = cv2.convexityDefects(contour, hull)
    perim_area_ratio = float(perimeter / area) if area != 0 else math.inf
    contour_perimeter_hull_perimeter_ratio = float(perimeter) / hull_perimeter if hull_perimeter != 0 else math.inf

    # The area score is a linear function that increases with area
    area_score = area

    # The perimeter score is based on how far off the perim_area_ratio is from an experimentally determined average value
    # for a hand
    perim_score = 10000 * math.pow(10000, -abs(perim_area_ratio - 0.055))

    score = area_score + perim_score
    return score


def get_n_largest_contours(frames, n):
    """
    Given a lits of frames, returns the n frames with the largest contours
    :param frames: A list of frames
    :param n: How many frames to return
    :return: A list of n contour objects, that are the n largest from the frames
    """

    largest_contours = []

    for f in frames:
        # Find contours of the filtered frame
        _, contours, _ = cv2.findContours(f, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Get the largest contour from each frame
        # Start with a small positive area to filter out tiny "dot" contours since this is likely noise
        # in the frane
        max_area = 100
        ci = -1
        for i in range(len(contours)):
            cnt = contours[i]
            area = cv2.contourArea(cnt)
            if area > max_area:
                max_area = area
                ci = i

        # If there are no contours, we do not want to append to the list so we continue
        if ci == -1:
            continue

        # Largest area contour
        cnt = contours[ci]
        largest_contours.append(cnt)

    if len(largest_contours) > 0:
        # Reverse so we get contours in decreasing order of area (largest area at the front)
        cntsSorted = sorted(largest_contours, key=lambda x: cv2.contourArea(x), reverse=True)
        max_index = min(n, len(cntsSorted))
        n_largest_contours = cntsSorted[0:max_index]

        return n_largest_contours
    else:
        return []


def get_center_of_hand_frame(frame):
    """
    Given a frame that contains the mask of a hand, returns the center of the hand
    :param frame: The frame with a hand mask
    :return: The position of the hand in the frame
    """
    erode_size = max(cv2.getTrackbarPos("Palm Erode size", "Hand Settings"), 1)

    erode_element = cv2.getStructuringElement(cv2.MORPH_RECT, (erode_size, erode_size))
    eroded_mask = cv2.erode(frame, erode_element)
    blur = cv2.GaussianBlur(eroded_mask, (11, 11), 3, 3)

    _, contours, _ = cv2.findContours(blur, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if contours is None or len(contours) == 0:
        return None

    # In case there's noise, take the largest contour
    contour = max(contours, key=cv2.contourArea)

    if contour is not None:
        return get_center_of_contour(contour)

    return None


def get_center_of_contour(contour):
    """
    Gets the center/mean point of a given contour
    :param contour: An opencv contour
    :return: Coordinates (x, y) of the contour if it exists, and None otherwise
    """
    if contour is None:
        return None

    moments = cv2.moments(contour)

    # The center point of the moment
    if moments['m00'] != 0:
        x = int(moments['m10'] / moments['m00'])  # cx = M10/M00
        y = int(moments['m01'] / moments['m00'])  # cy = M01/M00

        center = int(x), int(y)
        return center
    return None


def get_hand_fingers(contour, center):
    """
    Given the contour for a hand and its position, return the position of each finger and whether of not it is extended
    :param frame: The frame to draw on
    :param contour: The contour representing the hand. Must not be None
    :param center: The centre position of the hand, in the same set of coordinates as the defects and frame. Must not be None
    :return: A list of 5 triples, where each triple is (x, y, extended)
    """
    assert contour is not None
    assert center is not None

    # Find convex hull and defects
    hull = cv2.convexHull(contour, returnPoints=False)
    defects = cv2.convexityDefects(contour, hull)

    # Create a list of defect points
    defect_pts = []
    for i in range(defects.shape[0]):
        _, _, d, _ = defects[i, 0]
        pt = tuple(contour[d][0])
        defect_pts.append(pt)

    # Sort the defect points in non-ascending order of distance to the center of the hand
    defects_by_dist_to_center = defect_pts
    defects_by_dist_to_center.sort(key=lambda d: (d[0] - center[0]) ** 2 + (d[1] - center[1]) ** 2, reverse=True)

    # Find the average defect distance from the center of the hand
    defectDistToCenter = [math.hypot(d[0] - center[0], d[1] - center[1]) for d in defect_pts]
    averageDistFromCenter = sum(defectDistToCenter) / float(len(defectDistToCenter))

    # Now iterate through the defects again, and find the hull points that are next to deep "valleys" of defect points,
    # corresponding to an extended finger
    fpts = set()
    for i in range(defects.shape[0]):
        s, e, f, _ = defects[i, 0]
        start = tuple(contour[s][0])
        end = tuple(contour[e][0])
        point = tuple(contour[f][0])
        # Use the cross product to calculate the distance from the defect point to the nearest line of the convex hull
        dist = np.linalg.norm(
            np.cross((end[0] - start[0], end[1] - start[1]),
                     (start[0] - point[0], start[1] - point[1]))) / np.linalg.norm(
            (end[0] - start[0], end[1] - start[1]))
        start_dist_from_center = math.hypot(start[0] - center[0], start[1] - center[1])
        end_dist_from_center = math.hypot(end[0] - center[0], end[1] - center[1])

        # The ratio between the hypotenuse of the triangle formed by the start, end, and defect points, vs the other sides
        path_ratio = math.hypot(end[0] - start[0], end[1] - start[1]) / (
            math.hypot(end[0] - point[0], end[1] - point[1]) + math.hypot(start[0] - point[0], start[1] - point[1]))

        threshold = 15
        path_ratio_upper_threshold = 1.0

        if dist > threshold and path_ratio <= path_ratio_upper_threshold:
            if start_dist_from_center > averageDistFromCenter * 1.1:
                fpts.update([(start, dist)])
            if end_dist_from_center > averageDistFromCenter * 1.1:
                fpts.update([(end, dist)])

    finger_pts = [x[0] for x in fpts]
    finger_pts = combine_points_within_thresh(finger_pts, 20)

    # Sort by distance to center, and take the top 5 (since there shouldn't be more than 5 fingers
    finger_pts.sort(key=lambda d: (d[0] - center[0]) ** 2 + (d[1] - center[1]) ** 2, reverse=True)
    finger_pts = finger_pts[0:min(5, len(finger_pts))]

    fingers = []
    for f in finger_pts:
        fingers.append((int(f[0]), int(f[1]), False))

    return fingers


def combine_points_within_thresh(data, thresh):
    """
    Given a list of points, combines points that are within thresh distance of one another and replaces
    the points with the mean of the group of points (that are within thresh)
    :param data: A list of points
    :param thresh: The threshold distance
    :return: A list of points where points that are within thresh of one another are replaced by their mean
    """
    grouped_points = group_points_within_threshold(data, thresh)
    result = []
    for lst in grouped_points:
        if len(lst) > 1:
            point = average_points(lst)
        else:
            point = lst[0]

        result.append(point)
    return result


def tuple_in_list(data, val, thresh):
    """
    Returns True if data contains a tuple (that is not val) whose distance from val is within threshold
    :param data: The list of tuples to check
    :param val: The tuple to find values close to
    :param thresh: The threshold distance
    :return: True if there exists a tuple in data that is within threshold of val, and False otherwise
    """
    for d in data:
        if d != val and math.hypot(val[0] - d[0], val[1] - d[1]) <= thresh:
            return True

    return False


def average_points(data):
    """
    Returns the mean of a list of points
    :param data: The list of points to take the mean of. Must not be None and must not be empty
    :return: The mean of the list of given points in cartesian space
    """
    assert data is not None
    assert len(data) > 0

    sum_x = sum([x[0] for x in data])
    sum_y = sum([x[1] for x in data])
    x = sum_x / float(len(data))
    y = sum_y / float(len(data))
    result = (x, y)
    return result


def group_points_within_threshold(data, threshold):
    """
    Groups points from the given data if the points are within threshold of one another
    :param data: A list of points
    :param threshold: A double representing the threshold distance
    :return: A list of lists, where each list contains the elements within threshold of each other
    """
    result = []

    # First, add all points that are not within a dist of thresh from any other point
    individual_points = []
    for d in data:
        if not tuple_in_list(data, d, threshold):
            individual_points.append(d)

    # The remaining set of points that are not in the result, and are therefore close to some other point
    data_rest = [x for x in data if x not in individual_points]
    result = [[x] for x in individual_points]

    while len(data_rest) > 0:
        temp_result = [data_rest[0]]
        count = math.inf
        while count > 0:
            count = 0
            for i in range(1, len(data_rest)):
                d = data_rest[i]
                if d not in temp_result and tuple_in_list(temp_result, d, threshold):
                    temp_result.append(d)
                    count += 1

        # Remove the points in temp_result from data_rest
        data_rest = [x for x in data_rest if x not in temp_result]
        result.append(temp_result)

    return result


def is_hand_closed(fingers, centre):
    if fingers is None or len(fingers) == 0:
        return True
    return False


def draw_hand(frame, contour, fingers, centre):
    """
    A helper function to draw the attributes of a hand on the given frame
    :param frame: The frame to draw on
    :param contour: The contour representing the hand
    :param fingers: The points of the fingers for the hand
    :param centre: The center point of the hand
    """
    if fingers is not None:
        for f in fingers:
            cv2.circle(frame, (f[0], f[1]), 6, [0, 255, 255], -1)
    if contour is not None:
        cv2.drawContours(frame, contour, -1, (255, 255, 0), 4)
        hull = cv2.convexHull(contour, returnPoints=False)
        defects = cv2.convexityDefects(contour, hull)

        for i in range(defects.shape[0]):
            s, e, f, d = defects[i, 0]
            start = tuple(contour[s][0])
            end = tuple(contour[e][0])
            cv2.line(frame, start, end, [0, 255, 0], 1)

    if centre is not None:
        cv2.circle(frame, centre, 10, [170, 0, 50], 5)


def calc_fps(camera):
    """
    Given a camera object, calculates and returns the fps receives from this camera
    :param camera: The camera object to find the FPS for
    :return: an integer representing the fps of the camera
    """
    num_frames = 120

    start = time.time()
    for i in range(num_frames):
        camera.get_frame()
    end = time.time()

    time_seconds = end - start

    fps = num_frames / float(time_seconds)
    return fps
