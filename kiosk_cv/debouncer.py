from collections import deque


class Debouncer:
    """
    A class for a Debouncer object, that debounces boolean signals
    """

    def __init__(self, size):
        self.data = deque(maxlen=size)
        self.value = None

    def get_data(self, new_data):
        """
        Returns the debounced data given new data
        :param new_data: The new data to include in the debouncing
        :return: The debounced data using the previous data and new_data
        """
        self.data.append(new_data)

        # If the entire deque (all the data we consider for debouncing) is the same value, set our internal value to
        # that value (either True or False) and return it. If the data is not all the same, return our saved internal value
        all_data_same = all(x==self.data[0] for x in self.data)
        if all_data_same:
            self.value = self.data[0]

        return self.value
