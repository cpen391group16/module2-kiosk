/*
 * util.c
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#include "util.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define LAT_0 49.271772
#define LONG_0 -123.243340

// 800/LAT_START-LAT_END
#define X_LAT_MAGIC 49500

// 480/ABS(LON_END)-ABS(LON_START)
#define Y_LON_MAGIC 32000

int max(int a, int b) {
	return a > b ? a : b;
}

int min(int a, int b) {
	return a < b ? a : b;
}

int abs(int a) {
	if(a < 0)
		return -a;
	return a;
}

/* This function is meant only for the UBC map used in our implementation. It is useless for any other map */
Point convert_coord_to_x_y(char* lat, char* lon)
{
	char *gbg;
	Point p;

	p.x = (LAT_0 - strtod(lat,&gbg))*X_LAT_MAGIC;
	p.y = (fabs(strtod(lon,&gbg)) - fabs(LONG_0))*Y_LON_MAGIC;

	return p;
}

/*
 * string - max length 14
 *
 * */
void add_trailing_spaces_to_14(char* string){

	string[14] = '\0';

	int length = strlen(string);
	int index = 0;

	for(index = length; index < 14; index ++)
	{
		if(index >= length)
			string[index] = ' ';
	}

	return;
}

