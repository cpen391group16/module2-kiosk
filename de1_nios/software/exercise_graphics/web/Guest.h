/*
 * Ride.h
 *
 * This library contains the prototypes and structs for anything related to a Guest
 *
 * @author Timothy Leung
 */

#ifndef GUEST_H_
#define GUEST_H_


/* Guest Structure to store guest information */
typedef struct{
	char lat[15];
	char lon[15];
	char gps_timestamp[15];

} guest_t;

/*
 * @brief Blocking function that sends a wifi command to retrieve the guest information for guest #1
 *
 * This function will send the relevant serial command to the ESP8266 to retrieve a JSON formatted string of guest #1's location
 * If the serial response from the ESP8266 is invalid (ie. parsing error), it will wait 1 second before sending the serial command again.
 * When the response is successfully received and parsed into guest_t struct, the function will return.
 *
 * @param guest struct to store the parsed information
 */
void block_and_get_guest_information(guest_t *guest);

#endif /* GUEST_H_ */
