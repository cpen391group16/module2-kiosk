/*
 * gps.c
 *
 * GPS related implementation
 * Author: Timothy Leung
 */

#include "../util/cJSON.h"
#include "string.h"
#include <stdio.h>
#include "Guest.h"

/*
 * Parse guest gps json response into guest struct
 *
 * @param json string to parse
 * @param guest guest_t to parse to
 */
int parse_gps_loc(char* json, guest_t *guest)
{
	int status = 1;
	cJSON *guest_json = cJSON_Parse(json);
	if (guest_json == NULL){
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			printf("Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}

	const cJSON *lat_obj = cJSON_GetObjectItemCaseSensitive(guest_json, "lat");
	const cJSON *lon_obj = cJSON_GetObjectItemCaseSensitive(guest_json, "lon");
	const cJSON *gps_timestamp_obj = cJSON_GetObjectItemCaseSensitive(guest_json, "gps_timestamp");

	//printf("Timestamp:%s Lat:%s Lon:%s",gps_timestamp_obj->valuestring,lat_obj->valuestring,lon_obj->valuestring);

	strcpy(guest->lat,lat_obj->valuestring);
    strcpy(guest->lon,lon_obj->valuestring);
    strcpy(guest->gps_timestamp,gps_timestamp_obj->valuestring);

end:
	cJSON_Delete(guest_json);
	return status;
}


