/*
 * Guest.c
 *
 * This file contains the implementation of the prototypes in Guest.c
 *
 * @author Timothy Leung
 */
#include <string.h>
#include <stdio.h>
#include "Guest.h"
#include "../devices/WiFi.h"
#include "gps.h"

/* Header that prefixes the response from ESP8266 */
#define GUEST_HEADER "loc"

/*
 * @brief Blocking function that sends a wifi command to retrieve the guest information for guest #1
 *
 * This function will send the relevant serial command to the ESP8266 to retrieve a JSON formatted string of guest #1's location
 * If the serial response from the ESP8266 is invalid (ie. parsing error), it will wait 1 second before sending the serial command again.
 * When the response is successfully received and parsed into guest_t struct, the function will return.
 *
 * @param guest struct to store the parsed information
 */
void block_and_get_guest_information(guest_t *guest)
{
	char wifi_buffer[500];
	enum status{SENT, RESEND, VALID}status = RESEND;

	printf("Getting Guest Information\n");

	while(1){
		if(status == RESEND){
			usleep(1000000);
			sendandgetresponse_wifi("loc1", "", wifi_buffer);
			status = SENT;
		}

		if(strstr(wifi_buffer,GUEST_HEADER) && parse_gps_loc(wifi_buffer+3, guest)){
			/* Sucessfully parsed all rides from JSON string into rides structure, return*/
			break;
		}
		else{
			/* Recieved message was not parsed sucessfully */
			status = RESEND;
		}
	}
}
