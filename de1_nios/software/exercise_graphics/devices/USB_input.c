/*
 * USB_input.c
*/

#include <stdio.h>
#include "USB_input.h"
#include "../graphics/graphics_lib.h"

#define USB_INPUT_CONTROL (*(volatile unsigned char *)(0x84000210))
#define USB_INPUT_STATUS (*(volatile unsigned char *)(0x84000210))
#define USB_INPUT_TXDATA (*(volatile unsigned char *)(0x84000212))
#define USB_INPUT_RXDATA (*(volatile unsigned char *)(0x84000212))
#define USB_INPUT_BAUD (*(volatile unsigned char *)(0x84000214))

int click_release = 1;
int cursor_colour;


void init_USB_in(void)
{
	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	USB_INPUT_CONTROL = 0x15;

	// program baud rate generator to use 9600 baud
	// The low 3 bits must not be 001, 010, 011, 100
	USB_INPUT_BAUD = 0x05;
}

// the following function polls the 6850 to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read
int is_data_ready_usb(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE

	return 0x01 & USB_INPUT_STATUS;
}

char getbyte_usb( void )
{
	char x;
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!is_data_ready_usb()) {};
//	printf("Data received\n");

	// Read the received byte from 6850 RxData register.
	x = USB_INPUT_RXDATA & 0xff;
	//print_bitwise_char(x);
	return x;
}



/* Returns a touch struct which contains coordinates & touch status
 *
 * Returns: touch struct containing:
 * status which is an enum containing {NOCOMMAND, PRESSED or RELEASED}
 * point which is a struct containing x,y values when status is PRESSED OR RELEASED
 */
struct cursor cursor_status(){
	unsigned int a, b;
	cursor t;
	t.status = GARBAGE;
	if(is_data_ready_usb()) {

		/* Read first byte and check if it is a leading press/release byte */
		char type = getbyte_usb();

//		if((type & 0xff) == 0x81) {
		if((type & 0x83) == 0x81) {
			click_release = 0;
			t.status = NOCLICK;
//			printf("click\n");
		}
		else if((type & 0x83) == 0x80) {
			if (click_release == 0){
				t.status = CLICK;
				click_release = 1;
			} else {
				t.status = NOCLICK;
			}
//			printf("no click\n");
		} else if ((type & 0x83) == 0x82){
			click_release = 1;
			t.status = DRAG;
//			printf("drag\n");
		} else{
//			printf("Bad header  ");
//			print_bitwise_char(type);
			// Throw away byte if it is not the status header
			return t;
		}

		/* Get next four x,y bytes only if the previous byte was a touch/release byte*/
		char x_upper = getbyte_usb();
		char x_lower = getbyte_usb();
		char y_upper = getbyte_usb();
		char y_lower = getbyte_usb();
//		printf("X Upper: ");printf("%d  ", x_upper);print_bitwise_char(x_upper);
//		printf("X Lower: ");printf("%d  ", x_lower);print_bitwise_char(x_lower);
//		printf("Y Upper: ");printf("%d  ", y_upper);print_bitwise_char(y_upper);
//		printf("Y Lower: ");printf("%d  ", y_lower);print_bitwise_char(y_lower);


		a = ((x_upper & 0b11111111) << 7) | (x_lower & 0b01111111);
		b = ((y_upper & 0b11111111) << 7) | (y_lower & 0b01111111);
		t.point.x = a;
		t.point.y = b;

		int cursor_colour_value = ((type & 0b00011100) >> 2);

//		printf("%d many fingers are shown \n", cursor_colour_value);
		if (cursor_colour_value == 0){
			cursor_colour = YELLOW_GREEN;
		} else if (cursor_colour_value == 1){
			cursor_colour = LIME;
		} else if (cursor_colour_value == 2){
			cursor_colour = BLUE;
		} else if (cursor_colour_value == 3){
			cursor_colour = RED;
		} else if (cursor_colour_value == 4){
			cursor_colour = PALE_GOLDEN_ROD;
		}else if (cursor_colour_value == 5){
			cursor_colour = PURPLE;
		}

	}


	return t;
}

//struct cursor cursor_status(){
//	unsigned int a, b;
//	cursor t;
//	t.status = GARBAGE;
//	if(is_data_ready_usb()) {
//		char type = getbyte_usb();
//		print_hex_char(type);
////		print_bitwise_char(type);
//	}
//	return t;
//}
void print_bitwise_char (char a){
	  int i;
	  for (i = 0; i < 8; i++) {
		  printf("%d", !!((a << i) & 0x80));
	  }
	  printf("\n");
	}
void print_hex_char(unsigned char a) {
	printf("%x\n", a & 0xff);
}
void print_bitwise_int (int a){
	  int i;
	  for (i = 0; i < 32; i++) {
		  printf("%d", !!((a << i) & 0x8000));
	  }
	  printf("\n");
	}
