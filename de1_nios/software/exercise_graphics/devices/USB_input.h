/*
 * USB_input.h
 */

#ifndef USB_INPUT_H_
#define USB_INPUT_H_

#include "../util/geom.h"

enum cursor_status{GARBAGE, CLICK, NOCLICK, DRAG};

typedef struct cursor{
	enum cursor_status status;
	unsignedPoint point;
} cursor;

void init_USB_in(void);

int is_data_ready_usb(void);

char getbyte_usb( void );

struct cursor cursor_status();

void print_bitwise_char (char a);

void print_bitwise_int (int a);

#endif /* USB_INPUT_H_ */
