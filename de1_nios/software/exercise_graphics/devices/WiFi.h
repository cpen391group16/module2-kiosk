/*
 * WiFi.h
 *
 * Contains function prototypes for functions related to communicating with the ESP8266 through serial
 *
 * Author: Timothy Leung
 */

#ifndef WIFI_H_
#define WIFI_H_

/*
 * Initializes WiFi Serial UART coms with baudrate and other serial settings
 */
void init_wifi(void);

/*
 * Sends a single character through the WiFi TX
 *
 * @param c - character to send
 */
int sendchar_wifi(int c);

/*
 * Sends a string through the WiFi TX
 *
 * @param message string to send
 */
void sendstring_wifi(char* message);

/*
 * Concatenates header and payload and sends the resulting string through the WiFi TX
 *
 * @param header message, goes first
 * @param payload message, goes second
 */
void sendmessage_wifi(char* header, char* payload);

/*
 * Processes a single character if ready and places it into the WiFi Buffer and increases wifiMsgIndex.
 * If a '\r' is recieved, wifiMsgReady is set to 1.
 *
 * @param wifi_buffer Pointer to first char of char array to store next character recieved
 * @param wifi_msg_ready signals when a '\r' is recieved and the contents in wifi_buffer can be read
 * @param wifi_msg_index the index of wifi_buffer
 */
void process_wifi_buffer(char* wifi_buffer, int* wifi_msg_ready, int* wifi_msg_index);

/*
 * Sends header, payload terminted by '\r'. Waits for a '\r' terminted response from serial RX and places it it response
 *
 * @param header for string payload for serial TX
 * @param payload for serial TX
 * @param response the '/r' response from the ESP8266 will be placed here
 */
void sendandgetresponse_wifi(char* header, char* payload, char* response);


/*
 * Use this function upon initialization. It attempts to read a character from buffer if it exists. Returns if buffer is empty
 *
 */
void clear_buffer(void);
#endif /* WIFI_H_ */
