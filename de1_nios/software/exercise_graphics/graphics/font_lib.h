/*
 * fontlib.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#ifndef FONT_LIB_H_
#define FONT_LIB_H_

/*
 * This struct defines a font. It holds the width and height
 * of the font characters in pixels
 */
typedef struct Font {
	int width; // How wide each character is in pixels
	int height; // How tall each character is in pixels
	int buffer; // How many pixels should be between each character when writing strings
	int elementsPerChar;
	unsigned char* font_array;
} Font;

// Public variables corresponding to the fonts available
extern const Font TINY;
extern const Font SMALL;
extern const Font MEDIUM;
extern const Font LARGE;
extern const Font HUGE;

extern const unsigned char Font5x7[][7];
extern const unsigned short int Font10x14[][14];
extern const unsigned char Font16x27[];
extern const unsigned char Font22x40[];
extern const unsigned char Font38x59[];

/*************************************************************************************************
** This function draws a single ASCII character at the coord and colour specified
** it optionally ERASES the background colour pixels to the background colour
** This means you can use this to erase characters
**
** e.g. writing a space character with Erase set to true will set all pixels in the
** character to the background colour
**
*************************************************************************************************/
void draw_char(int x, int y, int fontcolour, int backgroundcolour, int c, Font font, int Erase);

/*
 * Draws a non-null string starting at the given coordinates with the given font
 */
void draw_string(char* string, int x, int y, int fontcolour, int backgroundcolour, Font font, int Erase);

/*
 * Returns the total length that a string with a given font will take up in pixels
 */
int get_string_pixel_width(char* string, Font font);

/*
 * Returns the total height a string with a given font will take up in pixels
 */
int get_string_pixel_height(char* string, Font font);

/*
 * An equality function for fonts. Returns true if the fonts are equal and false otherwise
 */
int font_eq(Font f1, Font f2);

#endif /* FONT_LIB_H_ */
