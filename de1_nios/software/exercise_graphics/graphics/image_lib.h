/*
 * fontlib.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Mathew
 */

#ifndef IMAGE_LIB_H_
#define IMAGE_LIB_H_

#include "Colours.h"

// Defines a container for an image that can be drawn on the screen
typedef struct Image {
	int width;
	int height;
	const unsigned char* img_array; // The array containing the pixel data for the image
	ColourPalette palette; // The color palette this image prefers to use when it is drawn
} Image;

// Draws the specified image at the given coordinates. The top-left corner of the image will
// be at the x, y coordinates.
void draw_image(int x, int y, Image img);

#endif /* IMAGE_LIB_H_ */
