import math

# The color palette to map image colors to. This is taken from whatever color palette is being used on the De1
NIOS_COLOR_PALETTE = [
0x00000000, # Black
0x00FFFFFF, # White
0x00FF0000, # Red
0x0000FF00, # Green/Lime
0x000000FF, # Blue
0x00FFFF00, # Yellow
0x0000FFFF, # Cyan
0x00FF00FF, # Magenta
0x00C0C0C0, # Silver
0x00808080, # Gray
0x00800000, # Maroon
0x00808000, # Olive
0x00008000, # DarkGreen
0x00800080, # Purple
0x00008080, # Teal
0x00000080, # Navy
0x008B0000, # Dark Red
0x00A52A2A, # Brown
0x00B22222, # FireBrick
0x00DC143C, # Crimson
0x00FF6347, # Tomato
0x00FF7F50, # Coral
0x00Cd5C5C, # Indian Red
0x00F08080, # Light Coral
0x00E9967A, # Dark Salmon
0x00FA8072, # Salmon
0x00FFA07A, # Light Salmon
0x00FF4500, # Orange Red
0x00FF8C00, # Dark Orange
0x00FFA500, # Orange
0x00FFD700, # Gold
0x00B8860B, # Dark Golden Rod
0x00DAA520, # Golden Rod
0x00EEE8AA, # Pale Golden Rod
0x00BDB76B, # Dark Kharki
0x00F0E68C, # Khaki
0x00808000, # Olive
0x00FFFF00, # Yellow
0x009ACD32, # Yellow Green
0x00556B2F, # Dark Olive Green
0x006B8E23, # Olive Drab
0x007CFC00, # Lawn Green
0x007FFF00, # Chart Reuse
0x00ADFF2F, # Green Yellow
0x00006400, # Dark Green
0x00008000, # Green
0x00228B22, # Forest Green
0x0000FF00, # Green/Lime
0x0032CD32, # Lime Green
0x0090EE90, # Light Green
0x0098FB98, # Pale Green
0x008FBC8F, # Dark See Green
0x0000FA9A, # Medium Spring Green
0x0000FF7F, # Spring Green
0x002E8B57, # Sea Green
0x0066CDAA, # Medium Aqua Marine
0x003CB371, # Medium Sea Green
0x0020B2AA, # Light Sea Green
0x002F4F4F, # Dark Slate Gray
0x00008080, # Teal
0x00008B8B, # Dark Cyan
0x0000FFFF, # Aqua/Cyan
0x00E0FFFF, # Light Cyan
0xFF000000, # Transparent/Clear
]

# Converts a number in hex into a r, g, b triple
def hex_to_rgb(value):
    r = (value >> 16) & 0xFF
    g = (value >> 8) & 0xFF
    b = (value) & 0xFF
    return (r, g, b)

# Returns the closest color value in the palette to the given color
def get_closest_val_in_palette_index(color):
    rc = hex_to_rgb(color)[0]
    gc = hex_to_rgb(color)[1]
    bc = hex_to_rgb(color)[2]

    closest_color = NIOS_COLOR_PALETTE[0]
    min_dist = None
    for i in range(len(NIOS_COLOR_PALETTE)):
        c = NIOS_COLOR_PALETTE[i] & 0xFFFFFF
        r = hex_to_rgb(c)[0]
        g = hex_to_rgb(c)[1]
        b = hex_to_rgb(c)[2]

        # We find the closest color in the colorpalette by treating the r, g, b space as 3-dimensional space, and finding
        # the shortes distance to another point in that space
        d = math.sqrt((r - rc)**2 + (g - gc)**2 + (b - bc)**2)
        if min_dist is None or d < min_dist:
            closest_color = c
            min_dist = d

    index = NIOS_COLOR_PALETTE.index(closest_color)
    return index