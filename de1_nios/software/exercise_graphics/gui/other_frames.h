/*
 * other_frames.h
 */

#ifndef OTHER_FRAMES_H_
#define OTHER_FRAMES_H_

#include "booth_graphic_interface.h"
/*
* Draws the starting frame.
*/
void starting_frame();


/*
* Checks wether start has been pushed.
*
* @Returns 1 if button has been pressed 0 else
*/

int start_button_check();
int start_button_check2(unsignedPoint p);

/*
* Draws the map frame.
*/
void map_frame();

/*
* Checks wether the back button in the map frame has been pressed.
* @Return 1 if true 0 else.
*/
int map_button_check();

int map_button_check2(unsignedPoint p);

/*
* Done button functionality.
* Sends off selected information to server.
*/
void done();

/*
* Checks screen for any touch and switches to starting frame if one is detected.
* @Return 1 if touch exists else 0.
*/
int exit_button_check();
int exit_button_check2(unsignedPoint p);

#endif
