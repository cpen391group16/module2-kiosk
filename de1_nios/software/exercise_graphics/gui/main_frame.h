/*
 * main_frame.h
 */


#ifndef MAIN_FRAME_H_
#define MAIN_FRAME_H_

#include "booth_graphic_interface.h"


/*
* Draws the main frame.
*/
void main_frame();

/*
* Writes the current information for ride i into the
* information box.
*/
void write_info(int i);

/*
* Highlights Ride1.
*/
void ride1_selected();

/*
* Locks in Ride1.
*/
void ride1_locked_in();

/*
* Highlights Ride2.
*/
void ride2_selected();

/*
* Locks in Ride2.
*/
void ride2_locked_in();

/*
* Highlights Ride3.
*/
void ride3_selected();

/*
* Locks in Ride3.
*/
void ride3_locked_in();

/*
* Highlights Ride4.
*/
void ride4_selected();

/*
* Locks in Ride4.
*/
void ride4_locked_in();

/*
* Deselects the currently selected ride/ removes highlight.
*/
void deselect();

/*
* Selects ride i and deselects previous ride.
*
* @Param i ride being selected.
*/
void coaster_selection(int i, unsignedPoint p);

/*
* Select Button functionality.
* Locks in selected ride.
*/
void select();

/*
* Undoes all selections made so rides can be reselected.
*/
void undo();


/*
* Called in main frame. Checks if there has been a touch,
* if the location fits any button and triggers resulting button functionality.
*/
void main_button_check();


void main_button_check2(unsignedPoint p);


void main_button_hover_check(unsignedPoint p);


#endif
