/*
 * main_frame.c
 */

#include "main_frame.h"
#include "objects.h"

#define LOCKED_RIDE_COLOUR GREEN

/*
* Draws the main frame.
*/
void main_frame(){
	frame = 2;
	int i;
	for (i = 1; i < 61; i++){
		rect(680 - 2*i , 120 - 2*i , i*4, i*4, MEDIUM_SPRING_GREEN, MEDIUM_SPRING_GREEN, 1, 1); // info box

		rect(680 - 2*i, 360 - 2*i, i*4, i*4, ORANGE, ORANGE, 1, 1); // finish/undo/select box

		rect( 240 - 4*i, 240 - 4*i, 80 + i*8, i*8, MAGENTA, MAGENTA, 1, 1);
	}
	char* string = "Select Ride";
	draw_string(string, 40, 30, NAVY, MAGENTA, LARGE, 0);


	// Ride information box
	string = "Name:";
	draw_string(string, 580, 10, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Ride Type:";
	draw_string(string, 580, 56, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Max Speed:";
	draw_string(string, 580, 102, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Length";
	draw_string(string, 580, 148, RED, MEDIUM_SPRING_GREEN, SMALL, 0);
	string = "Wait Time";
	draw_string(string, 580, 194, RED, MEDIUM_SPRING_GREEN, SMALL, 0);

	string = "Timetable";
	draw_string(string, 580, 260, BLUE, ORANGE, LARGE, 0);
	string = "Reset";
	draw_string(string, 580, 340, BLUE, ORANGE, LARGE, 0);
	string = "Finish";
	draw_string(string, 580, 420, BLUE, ORANGE, LARGE, 0);

	rect(360, 10, 190, 70, CYAN, CYAN, 1, 1);
	string = "View Park";
	draw_string(string, 365, 30, BLUE, CYAN, MEDIUM, 0);


	rect(40, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); //ride 1
	draw_image(55, 105, PENDULUM_RIDE);
	rect(40, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 2
	draw_image(55, 300, SWING_RIDE);
	rect(300, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 3
	draw_image(315, 105, ROLLERCOASTER);
	rect(300, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 4
	draw_image(315, 300, FERRIS_WHEEL);

	int colour;
	if (ride_1_selected){
		colour = LOCKED_RIDE_COLOUR;
	} else if (selected == 1) {
		colour = YELLOW_GREEN;
	} else {
		colour = LIGHT_SEA_GREEN;
	}
	rect(40, 90, 250, 185, colour, colour, -15, 0); //ride 1


	if (ride_2_selected){
			colour = LOCKED_RIDE_COLOUR;
	} else if (selected == 2) {
		colour = YELLOW_GREEN;
	} else {
		colour = LIGHT_SEA_GREEN;
	}
	rect(40, 285, 250, 185, colour, colour, -15, 0); // ride 2

	if (ride_3_selected){
		colour = LOCKED_RIDE_COLOUR;
	} else if (selected == 3) {
		colour = YELLOW_GREEN;
	} else {
		colour = LIGHT_SEA_GREEN;
	}
	rect(300, 90, 250, 185, colour, colour, -15, 0); // ride 3

	if (ride_4_selected){
		colour = LOCKED_RIDE_COLOUR;
	} else if (selected == 4) {
		colour = YELLOW_GREEN;
	} else {
		colour = LIGHT_SEA_GREEN;
	}
	rect(300, 285, 250, 185, colour, colour, -15, 0); // ride 4


	draw_cursor(cursor_information[CURSOR_SIZE*8], cursor_information[CURSOR_SIZE*8 + 1]);

	return;
}

/*
* Writes the current information for ride i into the 
* information box.
*/
void write_info(int i){
	// Ride Name
	char* string = rides[i].name;
	draw_string(string, 580, 30, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Type
	string = rides[i].ride_type;
	draw_string(string, 580, 76, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Speed
	string = rides[i].max_speed;
	draw_string(string, 580, 122, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Ride Length
	string = rides[i].length;
	draw_string(string, 580, 168, RED, MEDIUM_SPRING_GREEN, SMALL, 1);

	// Wait Time
	char temp[15];
	sprintf(&temp, "%d minutes    ", rides[i].wait_time);
	draw_string(temp, 580, 214, RED, MEDIUM_SPRING_GREEN, SMALL, 1);
	return;
}


/*
* Highlights Ride1.
*/
void ride1_selected(){ 
	if (!ride_1_selected){
	rect(40, 90, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0); 
	}
	write_info(0);
	return;

}

/*
* Locks in Ride1.
*/
void ride1_locked_in(){
	rect(40, 90, 250, 185, LOCKED_RIDE_COLOUR, LOCKED_RIDE_COLOUR, -15, 0);
	return;
}

/*
* Highlights Ride2.
*/
void ride2_selected(){ 
	if (!ride_2_selected){
		rect(40, 285, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0);
	}
	write_info(1);
	return;

}

/*
* Locks in Ride2.
*/
void ride2_locked_in(){
	rect(40, 285, 250, 185, LOCKED_RIDE_COLOUR, LOCKED_RIDE_COLOUR, -15, 0);
	return;
}

/*
* Highlights Ride3.
*/
void ride3_selected(){ 
	if (!ride_3_selected){
		rect(300, 90, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0);
	}
	write_info(2);
	return;

}

/*
* Locks in Ride3.
*/
void ride3_locked_in(){
	rect(300, 90, 250, 185, LOCKED_RIDE_COLOUR, LOCKED_RIDE_COLOUR, -15, 0);
	return;
}

/*
* Highlights Ride4.
*/
void ride4_selected(){ 
	if (!ride_4_selected){
		rect(300, 285, 250, 185, YELLOW_GREEN, YELLOW_GREEN, -15, 0);
	}
	write_info(3);
	return;
}

/*
* Locks in Ride4.
*/
void ride4_locked_in(){
	rect(300, 285, 250, 185, LOCKED_RIDE_COLOUR, LOCKED_RIDE_COLOUR, -15, 0);
	return;
}

/*
* Deselects the currently selected ride/ removes highlight.
*/
void deselect(){

	if (selected == 1 && !ride_1_selected) {
		rect(40, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); //ride 1
	} else if (selected == 2 && !ride_2_selected) {
		rect(40, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 2
	} else if (selected == 3 && !ride_3_selected) {
		rect(300, 90, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 3
	} else if (selected == 4 && !ride_4_selected) {
		rect(300, 285, 250, 185, LIGHT_SEA_GREEN, LIGHT_SEA_GREEN, -15, 0); // ride 4
	}
	return;
}

/*
* Selects ride i and deselects previous ride.
* 
* @Param i ride being selected.
*/
void coaster_selection(int i, unsignedPoint p){
	if (i == 1 && selected != 1) { //ride1
		redraw_back_cursor();
		ride1_selected();
		deselect();
		draw_cursor(p.x, p.y);
	}
	if (i == 2 && selected != 2) { //ride2
		redraw_back_cursor();
		ride2_selected();
		deselect ();
		draw_cursor(p.x, p.y);
	}
	if (i == 3 && selected != 3) { //ride3
		redraw_back_cursor();
		ride3_selected();
		deselect ();
		draw_cursor(p.x, p.y);
	}
	if (i == 4 && selected != 4) { //ride4
		redraw_back_cursor();
		ride4_selected();
		deselect ();
		draw_cursor(p.x, p.y);
	}
	selected = i;
	return;
}

/*
* Select Button functionality.
* Locks in selected ride.
*/
void select(){
		 //select
		if (selected == 1){
			ride1_locked_in();
			ride_1_selected = 1;
			selected = 0;
		} else if (selected == 2){
			ride2_locked_in();
			ride_2_selected = 1;
			selected = 0;
		}else if (selected == 3){
			ride3_locked_in();
			ride_3_selected = 1;
			selected = 0;
		}else if (selected == 4){
			ride4_locked_in();
			ride_4_selected = 1;
			selected = 0;
		}

		return;
}



/*
* Undoes all selections made so rides can be reselected.
*/
void undo(){
		if(ride_1_selected == 1){
			selected = 1;
			ride_1_selected = 0;
			deselect ();
		}
		if(ride_2_selected == 1){
			selected = 2;
			ride_2_selected = 0;
			deselect ();
			}
		if(ride_3_selected == 1){
			selected = 3;
			ride_3_selected = 0;
			deselect ();
		}
		if(ride_4_selected == 1){
			selected = 4;
			ride_4_selected = 0;
			deselect ();
		}
		selected = 0;

		unlock_timeslots();
}


/*
* Called in main frame. Checks if there has been a touch,
* if the location fits any button and triggers resulting button functionality.
*/


void main_button_check2(unsignedPoint p){

	if ( p.x <= 550){ // Ride Buttons + View Park
		if ( p.x >= 360 && p.y <= 90){ // View Park Button
			map_frame();
		} else {
			redraw_back_cursor();
			select();
			draw_cursor(p.x, p.y);
		}

	} else { // Side Bar
		if ( p.y >= 400){ // Finish
			done();
		} else if ( p.y >= 320){ // Reset
			undo();
		} else if (p.y >= 240){ // Select
			timetable_frame();
		}

	}
	return;
}

void main_button_hover_check(unsignedPoint p){
	if ( p.x <= 550){ // Ride Buttons + View Park
		if ( p.x <= 290){ // 1st Column

			if ( p.y >= 285){ // 2nd Row
				coaster_selection(2, p);
			} else if ( p.y >= 90){ // First Row
				coaster_selection(1, p);
			}
		} else { // 2nd Column

			if ( p.y >= 285 ){  // 2nd Row
				coaster_selection(4, p);
			} else if ( p.y >= 90){ // 1st Row
				coaster_selection(3, p);
			}
		}
	}
}
