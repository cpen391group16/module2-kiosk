/*
 * timetable_frame.h
 */
#ifndef TIMETABLE_FRAME_H_
#define TIMETABLE_FRAME_H_

#include "booth_graphic_interface.h"

typedef struct timeslot{
	int x;
	int y;
	int colour;
	int locked;
	int ride_number;
	int ticket_key;
} timeslot;

void refresh_timeslots();

void timetable_frame();

void timetable_interface(unsignedPoint p, int released);

void timetable_button_check2(unsignedPoint p);

void draw_grabbed_slot(int x, int y, int colour);

void move_grabbed_slot(int new_x, int new_y, int colour);

void redraw_background();

void timetable_done_check(unsignedPoint p);

void send_of_tickets();

void unlock_timeslots();

#endif
