/* 
 * id_frame.c
*/

#include "booth_graphic_interface.h"

#define KEY_COLOUR DARK_ORANGE
#define BACKGROUND_COLOUR LIGHT_GREEN
#define CHAR_COLOUR GOLD

char user_id[5];

void user_id_frame(){
	char* string;
	int i, y, x;

	rect(0, 0, 800, 480, BACKGROUND_COLOUR, BACKGROUND_COLOUR, 0, 1);
	frame = ID_FRAME;
	x = 90;
	y = 15;
	string = "Please enter your user ID";
	draw_string(string, x, y, KEY_COLOUR, CHAR_COLOUR, LARGE, 0);

	// first row  numbers
	// 1 2 3 4 5 6 7 8 9 0
	y = 150;
	x = 55;
	// char charArr[10] = {'1', '2', '3', '4', '5', '6', '7', '8', '9' ,'0'};
	string = "1234567890";
	for(i = 0; i < 10; i++){
		rect(x, y, 60, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
		draw_char(x + 19, y + 10, CHAR_COLOUR, KEY_COLOUR, *(string + i), LARGE, 0);
		x = x + 70;
	}

	// second row letters1
	// q w e r t y u i o p
	y = 230;
	x = 55;
	// charArr[10] = {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'};
	string = "qwertyuiop";
	for(i = 0; i < 10; i++){
		rect(x, y, 60, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
		draw_char(x + 19, y + 10, CHAR_COLOUR, KEY_COLOUR, *(string + i), LARGE, 0);
		x = x + 70;
	}

	// third row letters2
	// a s d f g h j k l delete
	y = 310;
	x = 65;
	// char charArr[10] = {'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'};
	string = "asdfghjkl";
	for(i = 0; i < 9; i++){
		rect(x, y, 60, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
		draw_char(x + 19, y + 10, CHAR_COLOUR, KEY_COLOUR, *(string + i), LARGE, 0);
		x = x + 70;
	}
	rect(x, y, 60, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
	x = x + 10;
	y = y + 10;
	draw_char(x, y, CHAR_COLOUR, KEY_COLOUR, '<', LARGE, 0);
	x = x + 10;
	draw_char(x, y, CHAR_COLOUR, KEY_COLOUR, '-', LARGE, 0);
	x = x + 10;
	draw_char(x, y, CHAR_COLOUR, KEY_COLOUR, '-', LARGE, 0);

	// fourth row letters3
	// z x c v b n m enter
	y = 390;
	x = 115;
	// char charArr[10] = {'z', 'x', 'c', 'v', 'b', 'n', 'm'};
	string = "zxcvbnm";
	for(i = 0; i < 7; i++){
		rect(x, y, 60, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
		draw_char(x + 19, y + 10, CHAR_COLOUR, KEY_COLOUR, *(string + i), LARGE, 0);
		x = x + 70;
	}
	rect(x, y, 120, 60, KEY_COLOUR, KEY_COLOUR, 0, 1);
	x = x + 10;
	y = y + 16;
	string = "enter";
	draw_string(string, x, y,CHAR_COLOUR, KEY_COLOUR, MEDIUM, 0);
	draw_cursor(50,50);

	return;
}

char user_id_button_check2(unsignedPoint p){
	int row = 0;
	int col = 0;

	if(p.y < 290){ // upper 2 rows
		if (p.y > 230) // row 2
			row = 2;
		else if (p.y > 150 && p.y < 210)
			row = 1;
		
		if (row){ // find col
			if(p.x > 405){ // right half
				if (p.x < 615){ // 6 7 or 8
					if (p.x > 545){ // 8
						col = 8;
					} else if (p.x > 475){ // 7
						col = 7;
					}
					else{ //6
						col = 6;
					}
				} else if (p.x < 755){ // 0 or 9
					if (p.x < 685){ // 9
						col = 9;
					} else if (p.x < 755) { // 0
						col = 10;
					}
					
				}
			} else if (p.x > 55){ // left half
				if (p.x < 265){ // 1 2 or 3
					if (p.x < 125){ // 1
						col = 1;
					} else if (p.x < 195){ // 2
						col = 2;
					} else { // 3
						col = 3;
					}
				} else if (p.x < 335){ // 4
					col = 4;
				} else { // 5
					col = 5;
				}	
			}
		}
	} else if (p.y > 310){ // lower 2 rows
		if (p.y < 370){ // row 3
			row = 3;

			if(p.x > 415){ // right half
				if (p.x < 625){ // 6 7 or 8
					if (p.x > 555){ // 8
						col = 8;
					} else if (p.x > 485){ // 7
						col = 7;
					}
					else{ //6
						col = 6;
					}
				} else if (p.x < 765){ // 0 or 9
					if (p.x < 695){ // 9
						col = 9;
					} else if (p.x < 755){ // 0
						col = 10;
					}
					
				}
			} else if (p.x > 55){ // left half
				if (p.x < 265){ // 1 2 or 3
					if (p.x < 125){ // 1
						col = 1;
					} else if (p.x < 195){ // 2
						col = 2;
					} else { // 3
						col = 3;
					}
				} else if (p.x < 335){ // 4
					col = 4;
				} else { // 5
					col = 5;
				}	
			}
		} else if (p.y < 450 && p.y > 390){ // row 4
			row = 4;

			if (p.x > 395){ // 5 6 7 or 8
				if (p.x > 535){ // 7 or 8
					if (p.x < 605){ // 7
						col = 7;
					} else if (p.x < 725){ // 8
						col = 8;
					}
				} else if (p.x > 465){ // 6
					col = 6;
				} else { // 5
					col = 5;
				}
			} else if (p.x > 115) { // 1 2 3 or 4
				if (p.x < 255){ // 1 or 2
					if (p.x < 185){ // 1
						col = 1;
					} else { // 2
						col = 2;
					}
				} else if (p.x < 325){ // 3 
					col = 3;
				} else { // 4
					col = 4;
				}
			}
		}
	}
	if (row && col){
		char* charArr[5] = {
							  {"1234567890"},
							  {"qwertyuiop"},
							  {"asdfghjkl}"},
							  {"zxcvbnm~"}
							};

		char e;
		e = *(charArr[row - 1] + col - 1);
		return e;
	}
	return 124;
}

void update_user_id_frame(char c){
	int i = c;

	if (i < 123 && user_id_cursor < 5){ // letter/number
		user_id[user_id_cursor] = c;
		draw_char(325 + user_id_cursor * 30, 80, KEY_COLOUR, BACKGROUND_COLOUR, c, LARGE, 1);
		user_id_cursor++;
	} else if ( i == 125 && user_id_cursor > 0){ // delete
		user_id_cursor--;
		draw_char(325 + user_id_cursor * 30, 80, KEY_COLOUR, BACKGROUND_COLOUR, 32, LARGE, 1);
	} else if ( i == 126 && user_id_cursor == 5){ // enter
		if(user_id[4] == '1'){
			user_value = 1;
			main_frame();
		} else if (user_id[4] == '2'){
			user_value = 2;
			main_frame();
		} else if (user_id[4] == '3'){
			user_value = 3;
			main_frame();
		} else if (user_id[4] == '4'){
			user_value = 4;
			main_frame();
		} else if (user_id[4] == '5'){
			user_value = 5;
			main_frame();
		} else if (user_id[4] == '6'){
			user_value = 6;
			main_frame();
		}
		user_id_cursor = 0;
	}
	return;
}
