/*
 * objects.h
 */

#ifndef OBJECTS_H_
#define OBJECTS_H_


#include "booth_graphic_interface.h"

extern int cursor_colour;

int cursor_information[CURSOR_SIZE*NUMBER_OF_CURSOR_LINES + 2];

void draw_cursor (int x, int y);

void redraw_back_cursor();

/*
 * moves cursor to new location
 */
void move_cursor (int new_x, int new_y);

/*
* Draws a black wheel with radius of 18 pixels.
*
* @Param x, y coordinates of lowest point of the wheel.
*/
void draw_wheel_r18(int x, int y);

/*
* Moves wheel drawn with draw_wheel_r18() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinates of lowest point of the wheel to be moved.
*/
void move_wheel_r18(int x, int y);

/*
* Moves rectangle at Location leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of rect
* @Param length, height of rectangle
* @Param colour of rectangle
*/
void move_rect(int x, int y, int length, int height, int colour);

/*
* Draws a coaster at specified position
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void draw_coaster(int position_x, int position_y);

/*
* Moves coaster drawn with draw_coaster() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void move_coaster(int position_x, int position_y);

#endif
