/*
 * booth_graphic_interface.h
 */
#ifndef BOOTH_GRAPHIC_INTERFACE_H_
#define BOOTH_GRAPHIC_INTERFACE_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../graphics/graphics_lib.h"
#include "../graphics/font_lib.h"
#include "../devices/lcd_touch.h"
#include "../devices/WiFi.h"
#include "../devices/USB_input.h"
#include "../graphics/image_lib.h"
#include "../graphics/images/ferris_wheel.h"
#include "../graphics/images/pendulum_ride.h"
#include "../graphics/images/swing_ride.h"
#include "../graphics/images/rollercoaster.h"
#include "../graphics/images/map.h"
#include "../web/Ride.h"
#include "../web/Guest.h"


#define CURSOR_SIZE 5
#define NUMBER_OF_CURSOR_LINES 12

/* selection flags */
extern int ride_1_selected;
extern int ride_2_selected;
extern int ride_3_selected;
extern int ride_4_selected;
extern int selected;

extern int user_id_cursor;
int user_value;

enum frame {EMPTY, START_FRAME, MAIN_FRAME, MAP_FRAME, END_FRAME, ID_FRAME, TIMETABLE_FRAME}frame;

extern int button_pressed;

extern ride_t rides[4];
extern int num_rides;
extern guest_t guest[1];


/*
*
* @Return the touch object retrieved
*/
touch get_touch();

void graphics();

#endif
