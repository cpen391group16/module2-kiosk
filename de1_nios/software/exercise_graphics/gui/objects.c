/*
 * Draws the cursor
 */

#include "objects.h"


#define CURSOR_SIZE 5
#define NUMBER_OF_CURSOR_LINES 12


int cursor_colour = BLACK;

void draw_cursor (int x, int y){
	int i;

	for (i = 0; i < CURSOR_SIZE; i++){

		cursor_information[i*NUMBER_OF_CURSOR_LINES] = read_a_pixel(x - 4 + i, y + i); //#1
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 1] = read_a_pixel(x - 4 + i, y + 1 + i); //#2
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 2] = read_a_pixel(x + i, y + 4 - i); //#3
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 3] = read_a_pixel(x + 1 + i, y + 4 - i); //#4
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 4] = read_a_pixel(x + 4 - i, y - i); //#5
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 5] = read_a_pixel(x + 4 - i, y - 1 - i); //#6
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 6] = read_a_pixel(x - i, y - 4 + i); //#7
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 7] = read_a_pixel(x - 1 - i, y - 4 + i); //#8

		cursor_information[i*NUMBER_OF_CURSOR_LINES + 8] = read_a_pixel(x - 5 + i, y + 1 + i); //#9
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 9] = read_a_pixel(x + 2 + i, y + 4 - i); //#10
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 10] = read_a_pixel(x + 5 - i, y - 1 - i);//#11
		cursor_information[i*NUMBER_OF_CURSOR_LINES + 11] = read_a_pixel(x - 2 - i, y - 4 + i);//#12


	}

	cursor_information[CURSOR_SIZE*NUMBER_OF_CURSOR_LINES] = x;
	cursor_information[CURSOR_SIZE*NUMBER_OF_CURSOR_LINES + 1] = y;


	for (i = 0; i < CURSOR_SIZE; i++){
		write_a_pixel(x - 4 + i, y + i, cursor_colour); //#1
		write_a_pixel(x - 4 + i, y + 1 + i, cursor_colour); //#2
		write_a_pixel(x + i, y + 4 - i, cursor_colour); //#3
		write_a_pixel(x + 1 + i, y + 4 - i, cursor_colour); //#4
		write_a_pixel(x + 4 - i, y - i, cursor_colour); //#5
		write_a_pixel(x + 4 - i, y - 1 - i, cursor_colour); //#6
		write_a_pixel(x - i, y - 4 + i, cursor_colour); //#7
		write_a_pixel(x - 1 - i, y - 4 + i, cursor_colour); //#8

		write_a_pixel(x - 5 + i, y + 1 + i, cursor_colour); //#9
		write_a_pixel(x + 2 + i, y + 4 - i, cursor_colour); //#10
		write_a_pixel(x + 5 - i, y - 1 - i, cursor_colour); //#11
		write_a_pixel(x - 2 - i, y - 4 + i, cursor_colour); //#12
	}

}

void redraw_back_cursor(){
	int i, x, y, l;
	x = cursor_information[CURSOR_SIZE*NUMBER_OF_CURSOR_LINES];
	y = cursor_information[CURSOR_SIZE*NUMBER_OF_CURSOR_LINES + 1];

	l = CURSOR_SIZE - 1;

	for (i = 0; i < CURSOR_SIZE; i++){
		write_a_pixel(x - l + i, y + i, cursor_information[i*NUMBER_OF_CURSOR_LINES]); //#1
		write_a_pixel(x - l + i, y + 1 + i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 1]); //#2
		write_a_pixel(x + i, y + l - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 2]); //#3
		write_a_pixel(x + 1 + i, y + l - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 3]); //#4
		write_a_pixel(x + l - i, y - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 4]); //#5
		write_a_pixel(x + l - i, y - 1 - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 5]); //#6
		write_a_pixel(x - i, y - l + i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 6]); //#7
		write_a_pixel(x - 1 - i, y - l + i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 7]); //#8

		write_a_pixel(x - 5 + i, y + 1 + i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 8]); //#9
		write_a_pixel(x + 2 + i, y + 4 - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 9]); //#10
		write_a_pixel(x + 5 - i, y - 1 - i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 10]); //#11
		write_a_pixel(x - 2 - i, y - 4 + i, cursor_information[i*NUMBER_OF_CURSOR_LINES + 11]); //#12
	}
}
/*
 * moves cursor to new location
 */
void move_cursor (int new_x, int new_y){

	redraw_back_cursor();
	draw_cursor (new_x, new_y);

	return;
}


/*
* Draws a black wheel with radius of 18 pixels.
*
* @Param x, y coordinates of lowest point of the wheel.
*/
void draw_wheel_r18(int x, int y){
	int i;
	int k;
	int m = 0;
	int h = 3;
	int j = 5;
	int g = 0;


	for (i = 0; i < 18; i++){


		k = -h;
		while( k <= h){
			write_a_pixel(x + k, y - i, BLACK);
			write_a_pixel(x + k, y - 35 + i, BLACK);
			k++;
		}

		if ( j > 1){
			j--;
			h = h + j;
		} else if ( m > 1){
			m--;

		} else {
			h = h + j;
			g++;
			m = g;
		}
	}
	return;
}

/*
* Moves wheel drawn with draw_wheel_r18() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinates of lowest point of the wheel to be moved.
*/
void move_wheel_r18(int x, int y){
	int i;
	int k;
	int m = 0;
	int h = 3;
	int j = 5;
	int g = 0;


	for (i = 0; i < 18; i++){


		k = -h;

		write_a_pixel(x + k - 1, y - i, BLACK);
		write_a_pixel(x + k - 1, y - 35 + i, BLACK);

		write_a_pixel(x + h, y - i, LIGHT_CYAN);
		write_a_pixel(x + h, y - 35 + i, BLUE);


		if ( j > 1){
			j--;
			h = h + j;
		} else if ( m > 1){
			m--;

		} else {
			h = h + j;
			g++;
			m = g;
		}
	}
	return;

}


/*
* Moves rectangle at Location leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of rect
* @Param length, height of rectangle
* @Param colour of rectangle
*/
void move_rect(int x, int y, int length, int height, int colour){
	v_line (x - 1, y - 1, height + 2, colour);
	v_line (x + length, y - 1, height + 2 , LIGHT_CYAN);
	return;
}


/*
* Draws a coaster at specified position
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void draw_coaster(int position_x, int position_y){
	int height = 60; 
	int length = 100; 

	rect(position_x + (length/6), position_y, length, height, BLUE, BLUE, 1, 1);

	// passenger 1
	rect(position_x + length/3 + 5, position_y - (height*1)/3 - 3, height/4, (height*1)/3, RED, RED, 1, 1); //body
	rect(position_x + length/3 - height/6, position_y - height/4 - 3, length/20, height/4, DARK_SLATE_GRAY, DARK_SLATE_GRAY, 1, 1); // handle
	circle(position_x + (length/3)+ length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*4)/5 - height/6, position_y - height, length/20, height/2, LIGHT_CYAN, LIGHT_CYAN, 1, 1);

	// passenger 2
	rect(position_x + (length*4)/5 + 5, position_y - (height*1)/3 - 3, height/4, (height*1)/3, RED, RED, 1, 1); //body
	rect(position_x + (length*4)/5 - height/6, position_y - height/4 - 3, length/30, height/4, DARK_SLATE_GRAY, DARK_SLATE_GRAY, 1, 1); // handle
	circle(position_x + (length*4)/5 + length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*5)/4 , position_y - height, length/20, height/2, LIGHT_CYAN, LIGHT_CYAN, 1, 1);


	draw_wheel_r18(position_x +(length*5)/12 ,position_y + 18 + height);
	draw_wheel_r18(position_x + (length*6)/7, position_y + 18 + height);

	return;
}


/*
* Moves coaster drawn with draw_coaster() leftward through the screen,
* background colour is white.
*
* @Param x, y coordinate of top left corner of actual wagon.
*/
void move_coaster(int position_x, int position_y){
	int height = 60;
	int length = 100;

	// cart
	move_rect(position_x + (length/6), position_y, length + 1, height, BLUE);
	move_wheel_r18(position_x +(length*5)/12 ,position_y + 18 + height);
	move_wheel_r18(position_x + (length*6)/7, position_y + 18 + height);
	// passenger 1
	move_rect(position_x + length/3 + 5, position_y - (height*1)/3 - 3, height/4 + 1, (height*1)/3, RED); //body
	move_rect(position_x + length/3 - height/6, position_y - height/4 - 3, length/30 + 1, height/4, DARK_SLATE_GRAY); // handle
	circle(position_x + (length/3)+ length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*4)/5 - height/6, position_y - height, length/20, height/2, LIGHT_CYAN, LIGHT_CYAN, 1, 1);

	// passenger 2
	move_rect(position_x + (length*4)/5 + 5, position_y - (height*1)/3 - 3, height/4 + 1, (height*1)/3, RED); //body
	move_rect(position_x + (length*4)/5 - height/6, position_y - height/4 - 3, length/30 + 1, height/4, DARK_SLATE_GRAY); // handle
	circle(position_x + (length*4)/5 + length/12, position_y - (height*9)/12, height/6, YELLOW, YELLOW,1 ,1); // head
	rect(position_x + (length*5)/4 , position_y - height, length/20, height/2, LIGHT_CYAN, LIGHT_CYAN, 1, 1);

	return;
}
