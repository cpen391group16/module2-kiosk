/*
 * booth_graphic_interface.c
 */


#include "booth_graphic_interface.h"
#include "timetable_frame.h"
#include "other_frames.h"
#include "objects.h"
#include "main_frame.h"
#include "id_frame.h"
#include "../devices/USB_input.c"
#include "id_frame.h"
#include "../devices/lcd_touch.h"
#include "id_frame.h"

/* selection flags */
int ride_1_selected = 0;
int ride_2_selected = 0;
int ride_3_selected = 0;
int ride_4_selected = 0;
int selected = 0;
int released = 1;

int user_id_cursor = 0;
int button_pressed;
int hover_counter = 0;

/*
*
* @Return the touch object retrieved
*/

touch get_touch(){
	struct touch t = touch_screen_status();
	if(t.status == PRESSED && button_pressed == 0){
		button_pressed = 1;
		printf("Button Pressed X: %d Y: %d\n", t.point.x, t.point.y);
		return t;
	}
	else if(t.status == RELEASED && button_pressed == 1){
		printf("Button Released\n");
		button_pressed = 0;
		return t;
	}

	t.status = NOCOMMAND;
	return t;
}


/*
* Main graphics loop that checks all buttons depending on frame and triggers
* all wanted graphics behavior
* Frame has to exist, can be called on any frame, non blocking, should be called in a loop.
*/
//void graphics(){
//	if (frame == START_FRAME){
//		start_button_check();
//	} else if (frame == MAIN_FRAME){
//		main_button_check();
//	} else if (frame == MAP_FRAME){
//		map_button_check();
//	} else if (frame == END_FRAME){
//		exit_button_check();
//	}
//	return;
//}
//
void graphics(){

	struct cursor c = cursor_status();
	if (c.status != GARBAGE){
//		printf("Valid Information received  \n");
//		printf(", X: %u, Y: %u \n", c.point.x, c.point.y);



		if (c.status == NOCLICK && released == 0){
			released = 1;
			timetable_interface(c.point, released);
		} else if (c.status == CLICK){
//			printf("click ");
//			printf(", X: %u, Y: %u \n", c.point.x, c.point.y);
			if (frame == START_FRAME){
				start_button_check2(c.point);
			} else if (frame == MAIN_FRAME){
//				printf(", X: %u, Y: %u \n", c.point.x, c.point.y);
				main_button_check2(c.point);
			} else if (frame == MAP_FRAME){
				map_button_check2(c.point);
			} else if (frame == END_FRAME){
				exit_button_check2(c.point);
			} else if (frame == ID_FRAME){
				update_user_id_frame(user_id_button_check2(c.point));
			} else if (frame == TIMETABLE_FRAME){
				timetable_done_check(c.point);
			}

		} else if (c.status == DRAG){
			if (frame == TIMETABLE_FRAME){
				released = 0;
				timetable_interface(c.point, released);
			}
		} else if (c.status == NOCLICK){
			if (frame == MAIN_FRAME){

//				if (hover_counter >= 30){
					main_button_hover_check(c.point);
//					hover_counter = 0;
//				}
//				hover_counter++;
			}
			move_cursor (c.point.x, c.point.y);
		}
	}
	return;
}

//void graphics(){
//
//	printf("draw grabbed slot: \n");
//	draw_grabbed_slot(400, 250, BLUE);
//	printf("redraw background");
////	redraw_background();
//
//	return;
//}
