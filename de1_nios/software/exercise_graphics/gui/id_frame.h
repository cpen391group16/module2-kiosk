/* 
 * id_frame.h
*/

#ifndef ID_FRAME_H_
#define ID_FRAME_H_

#include "booth_graphic_interface.h"

void user_id_frame();

char user_id_button_check2(unsignedPoint p);

void update_user_id_frame(char c);


#endif
