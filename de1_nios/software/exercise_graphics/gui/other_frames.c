/*
 * other_frames.c
 */

#include "other_frames.h"
#include "../web/Ride.h"
#include "../web/Guest.h"
#include "objects.h"
#include "main_frame.h"
#include "id_frame.h"

/*
* Draws the starting frame.
*/
void starting_frame(){
	clear(LIGHT_CYAN);
	ride_1_selected = 0;
	ride_2_selected = 0;
	ride_3_selected = 0;
	ride_4_selected = 0;
	selected = 0;

	frame = 1;
	int i;
	for (i = 0; i < 18; i++){
		h_line(0, 325 + i, 800, RED);
	}
	for (i = 0; i < 10; i++){
		h_line(0, 300 + i, 800, RED);
	}

	for (i = 0; i < 800; i = i + 20){
		rect(i, 309, 5, 15, RED, RED, 1, 1);
	}
	char* string = "START";
	draw_string(string, 305, 200, BLUE, LIGHT_CYAN, HUGE, 0);
	draw_coaster( 600, 222);


	return;
}

/*
* Checks wether start has been pushed.
*
* @Returns 1 if button has been pressed 0 else
*/

int start_button_check(){
	int i;
	button_pressed = 0;
	touch t = get_touch();
	Point p = t.point;
	if(t.status == PRESSED){
		if (p.x >= 300 && p.x <= 500 && p.y >= 200 && p.y <= 280){
			char* string = "START";
			draw_string(string, 305, 200, RED, LIGHT_CYAN, HUGE, 0);
			for (i = 0; i < 800; i++){
				move_coaster(600 - i, 222);
				if (i == 180){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'E', HUGE, 0);
				} else if ( i == 218){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'M', HUGE, 0);
				} else if ( i == 256){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'O', HUGE, 0);
				} else if ( i == 294){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'C', HUGE, 0);
				} else if ( i == 332){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'L', HUGE, 0);
				} else if ( i == 370){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'E', HUGE, 0);
				} else if ( i == 408){
					draw_char(715 - i , 230, BLUE, LIGHT_CYAN, 'W', HUGE, 0);
				}
			}
			user_id_frame();
			return 1;
		}
	}
	return 0;
}

int start_button_check2(unsignedPoint p){
	int i;
	button_pressed = 0;
	if (p.x >= 300 && p.x <= 500 && p.y >= 200 && p.y <= 280){
		printf("Start Ride Selection Button Hit \n");
		char* string = "START";
		draw_string(string, 305, 200, RED, WHITE, HUGE, 0);
		for (i = 0; i < 800; i++){
			move_coaster(600 - i, 222);
			if (i == 180){
				draw_char(715 - i , 230, BLUE, WHITE, 'E', HUGE, 0);
			} else if ( i == 218){
				draw_char(715 - i , 230, BLUE, WHITE, 'M', HUGE, 0);
			} else if ( i == 256){
				draw_char(715 - i , 230, BLUE, WHITE, 'O', HUGE, 0);
			} else if ( i == 294){
				draw_char(715 - i , 230, BLUE, WHITE, 'C', HUGE, 0);
			} else if ( i == 332){
				draw_char(715 - i , 230, BLUE, WHITE, 'L', HUGE, 0);
			} else if ( i == 370){
				draw_char(715 - i , 230, BLUE, WHITE, 'E', HUGE, 0);
			} else if ( i == 408){
				draw_char(715 - i , 230, BLUE, WHITE, 'W', HUGE, 0);
			}
		}
		user_id_frame();
		return 1;
	}
	return 0;
}




/*
* Draws the map frame.
*/
void map_frame(){
	frame = 3;
	clear(WHITE);

	draw_image(0, 0, MAP);

	int i;
	for(i = 0; i < num_rides;i++){
		marker(convert_coord_to_x_y(rides[i].lat ,rides[i].lon),rides[i].name, 2);
	}

	marker(convert_coord_to_x_y(guest->lat,guest->lon),"",4);
	
	// Legend
	rect(0,  400, 160, 80, WHITE, WHITE, 1, 1);
	legend_marker((Point){40,420}, "Ride", RED);
	legend_marker((Point){40,450}, "Guests", BLUE);

	// Back Button
	rect( 0,0 , 130, 80, TOMATO, TOMATO, 1, 1);
	char* string = "Back";
	draw_string(string, 40, 30, BLUE, ORANGE, MEDIUM, 0);

	draw_cursor (cursor_information[CURSOR_SIZE*8], cursor_information[CURSOR_SIZE*8 + 1]);

	return;
}

/*
* Checks wether the back button in the map frame has been pressed.
* @Return 1 if true 0 else.
*/
int map_button_check(){
	touch t = get_touch();
	Point p = t.point;
	if (t.status == PRESSED){
		if (p.x >= 0 && p.x <= 130 && p.y >= 0 && p.y <= 80){
			main_frame();
			return 1;
		} else {
			// update
			printf("Update Map");
		}
	}
	return 0;
}

int map_button_check2(unsignedPoint p){

	if (p.x >= 0 && p.x <= 130 && p.y >= 0 && p.y <= 80){
		main_frame();
		return 1;
	} else {
		printf("Update Map");
	}

	return 0;
}



/*
* Done button functionality.
* Sends off selected information to server.
*/
void done(){
	send_of_tickets();
	undo();
	clear(PURPLE);
	char* string = "Enjoy the Rides!";
	draw_cursor(50, 50);
	draw_string(string, 100, 200, WHITE, PURPLE, HUGE, 0);
	frame = 4;

	return;
}


/*
* Checks screen for any touch and switches to starting frame if one is detected.
* @Return 1 if touch exists else 0.
*/
int exit_button_check(){
	touch t = get_touch();
	Point p = t.point;
	if (t.status == PRESSED){
		if (p.x >= 0 && p.x <= 800 && p.y >= 0 && p.y <= 480){
			starting_frame();
			draw_cursor(0,0);
			return 1;
		}
	}
	return 0;
}

int exit_button_check2(unsignedPoint p){
	if (p.x >= 0 && p.x <= 800 && p.y >= 0 && p.y <= 480){
		starting_frame();
		draw_cursor(0,0);
		return 1;
	}

	return 0;
}
