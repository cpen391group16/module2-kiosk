/*
 * id_frame.c
 */

#include "timetable_frame.h"
#include "objects.h"
#include "id_frame.h"
#include "../devices/WiFi.h"
#include "../web/Ticket.h"


#define SLOT_HEIGHT 60
#define SLOT_WIDTH 90
#define MAX_TIMESLOTS 21
#define DRAG_OBJECT_SIZE 15
#define DRAG_SLOT_BORDER_THICKNESS 1
#define LOCKED_SLOT_Y 335

#define BACKGROUND_COLOUR LIGHT_CORAL
#define TABLE_COLOUR BROWN
#define STRING_COLOUR BROWN
#define DONE_BUTTON_COLOUR ORANGE
#define DONE_STRING_COLOUR BROWN
// x values
#define TWELVE_AM 235
#define ONE_PM 335
#define TWO_PM 435
#define THREE_PM 535
#define FOUR_PM 635
#define NUMBER_SLOTS_PER_RIDE 5
#define TIMESLOT_SHIFT 4

// y values
#define RIDE_ONE 50
#define RIDE_TWO 120
#define RIDE_THREE 190
#define RIDE_FOUR 260

/*
 * cursor stops drawing when empty drag (not grabbing a timelsot
 *
 * on release we have extra cursor draw somewhere
 */


int grabbed_x_shift;
int grabbed_y_shift;
struct timeslot* timeslots[MAX_TIMESLOTS];
int grabbed;
int grabbed_slot;
int drag_item_info[2* SLOT_WIDTH * DRAG_SLOT_BORDER_THICKNESS + 2 * (SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS) * DRAG_SLOT_BORDER_THICKNESS + 2];

void refresh_timeslots(){
	int ride_colours[4] = {YELLOW, RED, BLUE, WHITE};
	int ride_times[NUMBER_SLOTS_PER_RIDE] = {TWELVE_AM, ONE_PM, TWO_PM, THREE_PM, FOUR_PM};

	int ride_time;
	int ride_colour;
	struct timeslot* a;
	int e = 0;

	int i;
	for (i = 0; i < 4; i++){
		ride_time = ride_times[i];
		ride_colour = ride_colours[i];
		int k;
		for(k = TIMESLOT_SHIFT; k < NUMBER_SLOTS_PER_RIDE + TIMESLOT_SHIFT; k++){

			if (rides[i].timeslots[k]){
				ride_time = ride_times[k - TIMESLOT_SHIFT];
				a = malloc(sizeof(timeslot));
				a->x = ride_time;
				a->colour = ride_colour;
				a->locked = 0;
				a->ride_number = i + 1;
				a->ticket_key = rides[i].timeslots[k];
				timeslots[e] = a;
				e++;
			}
		}
	}
	timeslots[e] = NULL;

}
void timetable_frame(){
	int i, x, y;
	grabbed = 0;
	frame = TIMETABLE_FRAME;

	rect(0, 0, 800, 480, BACKGROUND_COLOUR, BACKGROUND_COLOUR, 0, 1);

	y = 50;
	int y_ride1, y_ride2, y_ride3 ,y_ride4;

	i = 0;
	if (ride_1_selected){
		y_ride1 = y;
		y = y + 70;
		i++;

	}
	if (ride_2_selected){
		y_ride2 = y;
		y = y + 70;
		i++;
	}
	if (ride_3_selected){
		y_ride3 = y;
		y = y + 70;
		i++;
	}
	if (ride_4_selected){
		y_ride4 = y;
		y = y + 70;
		i++;
	}
	int y_add = ((4 - i)%4)* 35;

	y_ride1 = y_ride1 + y_add;
	y_ride2 = y_ride2 + y_add;
	y_ride3 = y_ride3 + y_add;
	y_ride4 = y_ride4 + y_add;

	x = 60;
	y = 45;
	y = y + y_add;

	for (i = 0; i < MAX_TIMESLOTS; i++){
		if (timeslots[i] == NULL){
			break;
		} else if (timeslots[i]->ride_number == 1){
			timeslots[i]->y = y_ride1;
		} else if (timeslots[i]->ride_number == 2){
			timeslots[i]->y = y_ride2;
		} else if (timeslots[i]->ride_number == 3){
			timeslots[i]->y = y_ride3;
		} else if (timeslots[i]->ride_number == 4){
			timeslots[i]->y = y_ride4;
		}
	}


	if (ride_1_selected){
		y = y + 70;
		draw_string(rides[0].name, x, y - 49, STRING_COLOUR, BACKGROUND_COLOUR, MEDIUM, 0);
		h_line(x, y, 670, TABLE_COLOUR);
	}
	if (ride_2_selected){
		y = y + 70;
		draw_string(rides[1].name, x, y - 49, STRING_COLOUR, BACKGROUND_COLOUR, MEDIUM, 0);
		h_line(x, y, 670, TABLE_COLOUR);
	}
	if (ride_3_selected){
		y = y + 70;
		draw_string(rides[2].name, x, y - 49, STRING_COLOUR, BACKGROUND_COLOUR, MEDIUM, 0);
		h_line(x, y, 670, TABLE_COLOUR);
	}
	if (ride_4_selected){
		y = y + 70;
		draw_string(rides[3].name, x, y - 49, STRING_COLOUR, BACKGROUND_COLOUR, MEDIUM, 0);
		h_line(x, y, 670, TABLE_COLOUR);
	}
	h_line(x, y, 670, BACKGROUND_COLOUR);


	for (i = 0; i < MAX_TIMESLOTS; i++){
		if (timeslots[i] == NULL){
			break;
		} else if (timeslots[i]->locked){
			rect (timeslots[i]->x, LOCKED_SLOT_Y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[i]->colour, timeslots[i]->colour, 0, 1);
		} else if (timeslots[i]->ride_number == 1 && ride_1_selected){
			rect (timeslots[i]->x, timeslots[i]->y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[i]->colour, timeslots[i]->colour, 0, 1);
		} else if (timeslots[i]->ride_number == 2 && ride_2_selected){
			rect (timeslots[i]->x, timeslots[i]->y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[i]->colour, timeslots[i]->colour, 0, 1);
		} else if (timeslots[i]->ride_number == 3 && ride_3_selected){
			rect (timeslots[i]->x, timeslots[i]->y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[i]->colour, timeslots[i]->colour, 0, 1);
		} else if (timeslots[i]->ride_number == 4 && ride_4_selected){
			rect (timeslots[i]->x, timeslots[i]->y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[i]->colour, timeslots[i]->colour, 0, 1);
		}
	}

	y = 325;
	draw_string("Your Day", x, y + 21, STRING_COLOUR, BACKGROUND_COLOUR, MEDIUM, 0);
	h_line(x, y, 670, TABLE_COLOUR);
	y++;
	h_line(x, y, 670, TABLE_COLOUR);
	y++;
	h_line(x, y, 670, TABLE_COLOUR);

	x = 230;
	y = 45;
	for (i = 0; i < 5; i++){
		v_line(x, y, 353, TABLE_COLOUR);
		x = x + 100;
	}

	// done button
	y = 420;
	x = 0;
	rect (x, y, 800, 70, DONE_BUTTON_COLOUR, DONE_BUTTON_COLOUR, 0, 1);

	x = 390;
	y = y + 20;
	char* string = "DONE";
	draw_string(string, x, y, DONE_STRING_COLOUR, DONE_BUTTON_COLOUR, MEDIUM, 0);






	char* nine_am = "9AM ";
	char* ten_am = "10AM";
	char* eleven_am = "11AM";
	char* twelve_pm = "12PM";
	char* one_pm = "1PM ";
	char* two_pm = "2PM ";
	char* three_pm = "3PM ";
	char* four_pm = "4PM ";
	char* five_pm = "5PM ";
	char* six_pm = "6PM ";
	char* seven_pm = "7PM ";
	char* eight_pm = "8PM ";
	char* arr[12];
	arr[0] = nine_am;
	arr[1] = ten_am;
	arr[2] = eleven_am;
	arr[3] = twelve_pm;
	arr[4] = one_pm;
	arr[5] = two_pm;
	arr[6] = three_pm;
	arr[7] = four_pm;
	arr[8] = five_pm;
	arr[9] = six_pm;
	arr[10] = seven_pm;
	arr[11] = eight_pm;



	x = 215;
	y = 28;

	for (i = TIMESLOT_SHIFT; i < 5 + TIMESLOT_SHIFT; i++){
		draw_string(arr[i], x, y, STRING_COLOUR, BACKGROUND_COLOUR, SMALL, 0);
		x = x + 100;
	}

	draw_cursor(50,50);


}

void timetable_interface(unsignedPoint p, int released){

	if (grabbed){
		if (released){
			grabbed = 0;
			redraw_background();
			int i = read_a_pixel(timeslots[grabbed_slot]->x, LOCKED_SLOT_Y);
			if (p.y > LOCKED_SLOT_Y - 10 && (BACKGROUND_COLOUR == i)){
				rect (timeslots[grabbed_slot]->x, LOCKED_SLOT_Y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[grabbed_slot]->colour, timeslots[grabbed_slot]->colour, 0, 1);
				timeslots[grabbed_slot]->locked = 1;
			} else {
				rect (timeslots[grabbed_slot]->x, timeslots[grabbed_slot]->y, SLOT_WIDTH, SLOT_HEIGHT, timeslots[grabbed_slot]->colour, timeslots[grabbed_slot]->colour, 0, 1);
				timeslots[grabbed_slot]->locked = 0;
			}
			draw_cursor(p.x, p.y);
		} else {
			move_grabbed_slot(p.x - grabbed_x_shift, p.y - grabbed_y_shift, timeslots[grabbed_slot]->colour);
		}
	} else {
		timetable_button_check2(p);
	}
	
}

void timetable_button_check2(unsignedPoint p){
	int i;

	for (i = 0; i < MAX_TIMESLOTS; i++){
		if (timeslots[i] == NULL){
			break;
		} else if (!timeslots[i]->locked){
			if (timeslots[i]->y < p.y && (timeslots[i]->y + SLOT_HEIGHT) > p.y){
				if (timeslots[i]->x < p.x && (timeslots[i]->x + SLOT_WIDTH) > p.x){
					redraw_back_cursor();
					grabbed = 1;
					rect(timeslots[i]->x, timeslots[i]->y, SLOT_WIDTH, SLOT_HEIGHT, BACKGROUND_COLOUR, BACKGROUND_COLOUR, 0, 1);
					grabbed_slot = i;

					grabbed_x_shift = p.x - timeslots[i]->x;
					grabbed_y_shift = p.y - timeslots[i]->y;

					draw_grabbed_slot(p.x - grabbed_x_shift, p.y - grabbed_y_shift, timeslots[i]->colour);
					return;
				}
			}
		}else if (p.y > LOCKED_SLOT_Y){
			if (LOCKED_SLOT_Y < p.y && (LOCKED_SLOT_Y + SLOT_HEIGHT) > p.y){
				if (timeslots[i]->x < p.x && (timeslots[i]->x + SLOT_WIDTH) > p.x){
					redraw_back_cursor();
					grabbed = 1;
					rect(timeslots[i]->x, LOCKED_SLOT_Y, SLOT_WIDTH, SLOT_HEIGHT, BACKGROUND_COLOUR, BACKGROUND_COLOUR, 0, 1);
					grabbed_slot = i;

					grabbed_x_shift = p.x - timeslots[i]->x;
					grabbed_y_shift = p.y - LOCKED_SLOT_Y;

					draw_grabbed_slot(p.x - grabbed_x_shift, p.y - grabbed_y_shift, timeslots[i]->colour);
					return;
				}
			}
		}
	}
	move_cursor(p.x, p.y);
	return;
}

void draw_grabbed_slot(int x, int y, int colour){
	int i = 0;
	int k, t;

	for (k = 0; k < SLOT_WIDTH; k++){
		for (t = 0; t < DRAG_SLOT_BORDER_THICKNESS; t++){
			drag_item_info[i] = read_a_pixel(x + k, y + t);
			i++;
			drag_item_info[i] = read_a_pixel(x + k, y + t + SLOT_HEIGHT - DRAG_SLOT_BORDER_THICKNESS);
			i++;
		}
	}

	for (k = 0; k < SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS; k++){
		for (t = 0; t < DRAG_SLOT_BORDER_THICKNESS; t++){
			drag_item_info[i] = read_a_pixel(x + t, y + DRAG_SLOT_BORDER_THICKNESS + k);
			i++;
			drag_item_info[i] = read_a_pixel(x + t + SLOT_WIDTH - DRAG_SLOT_BORDER_THICKNESS, y + DRAG_SLOT_BORDER_THICKNESS + k);
			i++;
		}
	}

	int h = 2* SLOT_WIDTH * DRAG_SLOT_BORDER_THICKNESS + 2 * (SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS) * DRAG_SLOT_BORDER_THICKNESS;
	drag_item_info[h] = x;
	drag_item_info[h + 1] = y;

	rect(x + DRAG_SLOT_BORDER_THICKNESS - 1, y + DRAG_SLOT_BORDER_THICKNESS - 1, SLOT_WIDTH - 2*DRAG_SLOT_BORDER_THICKNESS + 1, SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS + 1, colour, colour, DRAG_SLOT_BORDER_THICKNESS - 1, 0);
}

void move_grabbed_slot(int new_x, int new_y, int colour){
	
	redraw_background();
	draw_grabbed_slot(new_x, new_y, colour);
}


void redraw_background(){
	int x, y, k, h, t;
	int i = 0;

	h = 2* SLOT_WIDTH * DRAG_SLOT_BORDER_THICKNESS + 2 * (SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS) * DRAG_SLOT_BORDER_THICKNESS;
	x = drag_item_info[h];
	y = drag_item_info[h + 1];

	for (k = 0; k < SLOT_WIDTH; k++){
		for (t = 0; t < DRAG_SLOT_BORDER_THICKNESS; t++){
			write_a_pixel(x + k, y + t, drag_item_info[i]);
			i++;
			write_a_pixel(x + k, y + t + SLOT_HEIGHT - DRAG_SLOT_BORDER_THICKNESS, drag_item_info[i]);
			i++;
		}
	}

	for (k = 0; k < SLOT_HEIGHT - 2*DRAG_SLOT_BORDER_THICKNESS; k++){
		for (t = 0; t < DRAG_SLOT_BORDER_THICKNESS; t++){
			write_a_pixel(x + t, y + DRAG_SLOT_BORDER_THICKNESS + k, drag_item_info[i]);
			i++;
			write_a_pixel(x + t + SLOT_WIDTH - DRAG_SLOT_BORDER_THICKNESS, y + DRAG_SLOT_BORDER_THICKNESS + k, drag_item_info[i]);
			i++;
		}
	}


	return;
}

void timetable_done_check(unsignedPoint p){
	if (p.y > 420){
		main_frame();
	}
	return;
}

void send_of_tickets(){
	int i = 0;
	while(timeslots[i] != NULL){
		if (timeslots[i]->locked){
			sendmessage_wifi("tic", new_ticket(user_value,timeslots[i]->ticket_key));
		}
		i++;
	}
	return;
}

void unlock_timeslots(){
	int i = 0;

	while(timeslots[i] != NULL){
		if(timeslots[i]->locked){
			timeslots[i]->locked = 0;
		}
		i++;
	}
	return;
}













