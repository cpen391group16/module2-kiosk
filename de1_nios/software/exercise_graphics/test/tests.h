#ifndef TESTS_H_
#define TESTS_H_

/**
 * This is a custom testing-framework and test suite, designed to test C code running on the NIOS II processor.
 *
 * Tests are divided into "modules", or the component that is to be tested as a whole. For example, utility functions,
 * geom functions, or the graphics library functions. Each test module has its own .h and .c file, which contain the
 * individual test functions as well as a function to run all the tests for a module.
 *
 * Test functions return 1 if the test fails, and return 0 otherwise. This allows for test failures to propagate and be reported
 * at various levels.
 *
 * Each test function is responsible for reporting its own failures with an error message. Test modules only report if one of their
 * tests fail.
 */

// Run all the tests in the testsuite. Return 1 if any tests fail, and 0 otherwise
int run_tests(void);

#endif
