#include "graphics_test.h"

#include <stdio.h>
#include "../graphics/graphics_lib.h"

// The main function for running the "test suite"
// Other tests can be called and managed from here
int run_graphics_tests(void) {
	// Make sure that we clear the screen before running tests so we know for sure what
	// the values of the pixels are beforehand. This is also an implicit test of the
	// rect() function, which is part of clear's implementation
	clear(BLACK);

	// Since all test functions return 1 if they fail, we bitwise-OR all the results
	// of the test functions to detect if any function failed. If so, we can report
	// that this test suite failed
	int test_failed =
			write_pixel_test() |
			h_line_test() |
			v_line_test() |
			line_test() |
			rectangle_test() |
			triangle_test() |
			circle_test();

	if(test_failed) {
		printf("graphics_tests failed. Check logs for more info\n");
	}

	return test_failed;
}

int write_pixel_test(void) {
	int expected_color = RED;

	// Write the pixels
	int i;
	for(i = 0; i < 100; i++) {
		write_a_pixel(i, i, expected_color);
	}

	// Validate all pixels were written properly
	for(i = 0; i < 100; i++) {
		int color = read_a_pixel(i, i);
		if(color != RED) {
			printf("Write_a_pixel failed: Pixel (%d, %d) was %d, expected %d\n", i, i, color, expected_color);
			return 1;
		}
	}

	return 0;
}

int h_line_test(void) {
	int x = 100;
	int y = 200;
	int length = 250;
	int expected_color = WHITE;

	h_line(x, y, length, expected_color);

	int i;
	for(i = x; i < x + length; i++) {
		int color = read_a_pixel(i, y);
		if(color != expected_color) {
			printf("h_line_test failed: pixel (%d, %d) was %d, expected %d\n", i, y, color, expected_color);
			return 1;
		}
	}

	return 0;
}

int v_line_test(void) {
	int x = 100;
	int y = 200;
	int length = 250;
	int expected_color = WHITE;

	v_line(x, y, length, expected_color);

	int i;
	for(i = y; i < y + length; i++) {
		int color = read_a_pixel(x, i);
		if(color != expected_color) {
			printf("h_line_test failed: pixel (%d, %d) was %d, expected %d\n", x, i, color, expected_color);
			return 1;
		}
	}

	return 0;
}

int line_test(void) {
	// Test diagonal lines
	int x1 = 100;
	int y1 = 100;
	int x2 = 400;
	int y2 = 400;
	int expected_color = BLUE;

	line(x1, y1, x2, y2, expected_color);

	int i;
	for(i = x1; i < x2; i++) {
		int color = read_a_pixel(i, i);
		if(color != expected_color) {
			printf("line_test failed: pixel (%d, %d) was %d, expected %d\n", i, i, color, expected_color);
			return 1;
		}
	}

	// Make sure the line function can also draw straight lines that are not a diagonal
	x1 = 200;
	y1 = 300;
	x2 = 200;
	y2 = 400;
	expected_color = RED;

	line(x1, y1, x2, y2, expected_color);

	for(i = y1; i < y2; i++) {
		int color = read_a_pixel(x1, i);
		if(color != expected_color) {
			printf("line_test failed: pixel (%d, %d) was %d, expected %d\n", x1, i, color, expected_color);
			return 1;
		}
	}

	return 0;
}

int rectangle_test(void) {
	int x = 100;
	int y = 100;
	int width = 50;
	int height = 100;
	int expected_color = BLUE;
	int expected_background_color = RED;
	int borderThickness = 15;
	int fill = 1;

	// Draw a filled rectangle with a border. Both need to be checked.
	rect(x, y, width, height, expected_color, expected_background_color, borderThickness, fill);

	int i;
	int j;
	for(i = x; i < x + width; i++) {
		for(j = y; j < y + height; j++) {
			int color = read_a_pixel(i, i);

			// First check if we are checking the border or not, then check if the color is correct.
			// This lets us validate the border at the same time as the rest of the rectangle
			if(i > x && i < x + width && j >= y && j < y + height) {
				 if(color != expected_color) {
					 printf("rectangle_test failed: fill pixel (%d, %d) was %d, expected %d\n", i, j, color, expected_color);
				 	 return 1;
				 }
			}else {
				if(color != expected_background_color) {
					 printf("rectangle_test failed: border pixel (%d, %d) was %d, expected %d\n", i, j, color, expected_background_color);
					 return 1;
				 }
			}
		}
	}

	return 0;
}

int triangle_test(void) {
	Point p1 = {400, 400};
	Point p2 = {500, 300};
	Point p3 = {600, 400};
	int expected_color = WHITE;

	triangle_points(p1, p2, p3, expected_color, 1);

	// Check that the triangle vertices were set properly
	int color = read_a_pixel(p1.x, p1.y);
	if(color != expected_color) {
		printf("triangle_test failed. pixel (%d, %d) was %d, expected %d\n", p1.x, p1.y, color, expected_color);
		return 1;
	}

	color = read_a_pixel(p2.x, p2.y+1);
	if(color != expected_color) {
		printf("triangle_test failed. pixel (%d, %d) was %d, expected %d\n", p2.x, p2.y+1, color, expected_color);
		return 1;
	}

	color = read_a_pixel(p3.x, p3.y);
	if(color != expected_color) {
		printf("triangle_test failed. pixel (%d, %d) was %d, expected %d\n", p3.x, p3.y, color, expected_color);
		return 1;
	}

	return 0;
}

int circle_test(void) {
	int x = 300;
	int y = 300;
	int radius = 100;
	int expected_color = RED;
	int borderColour = BLUE;
	int borderThickness = 20;
	int fill = 1;

	circle(x, y, radius, expected_color, borderColour, borderThickness, fill);

	// Check the center of the circle
	int color = read_a_pixel(x, y);
	if(color != expected_color) {
		printf("circle_test failed. pixel (%d, %d) was %d, expected %d\n", x, y, color, expected_color);
		return 1;
	}

	// Check 4 edge points on the main circle (not the border)
	color = read_a_pixel(x+radius, y);
	if(color != expected_color) {
		printf("circle_test failed. pixel (%d, %d) was %d, expected %d\n", x+radius, y, color, expected_color);
		return 1;
	}

	color = read_a_pixel(x-radius, y);
	if(color != expected_color) {
		printf("circle_test failed. pixel (%d, %d) was %d, expected %d\n", x-radius, y, color, expected_color);
		return 1;
	}

	color = read_a_pixel(x, y+radius);
	if(color != expected_color) {
		printf("circle_test failed. pixel (%d, %d) was %d, expected %d\n", x, y+radius, color, expected_color);
		return 1;
	}

	color = read_a_pixel(x, y-radius);
	if(color != expected_color) {
		printf("circle_test failed. pixel (%d, %d) was %d, expected %d\n", x, y-radius, color, expected_color);
		return 1;
	}

	// Check 4 edge points on the border
	color = read_a_pixel(x+radius+borderThickness-1, y);
	if(color != borderColour) {
		printf("circle_test failed. border pixel (%d, %d) was %d, expected %d\n", x+radius+borderThickness-1, y, color, borderColour);
		return 1;
	}

	color = read_a_pixel(x-radius-borderThickness+1, y);
	if(color != borderColour) {
		printf("circle_test failed. border pixel (%d, %d) was %d, expected %d\n", x-radius-borderThickness+1, y, color, borderColour);
		return 1;
	}

	color = read_a_pixel(x, y+radius+borderThickness-1);
	if(color != borderColour) {
		printf("circle_test failed. border pixel (%d, %d) was %d, expected %d\n", x, y+radius+borderThickness-1, color, borderColour);
		return 1;
	}

	color = read_a_pixel(x, y-radius-borderThickness+1);
	if(color != borderColour) {
		printf("circle_test failed. border pixel (%d, %d) was %d, expected %d\n", x, y-radius-borderThickness+1, color, borderColour);
		return 1;
	}

	return 0;
}
