#ifndef UTIL_TEST_H_
#define UTIL_TEST_H_

// The main function to run all graphics tests
int run_util_tests(void);

// The individual graphics tests. Tests should be separated by component
int max_test(void);
int min_test(void);
int abs_test(void);

#endif
