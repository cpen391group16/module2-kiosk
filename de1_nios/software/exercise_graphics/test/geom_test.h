#ifndef GEOM_TEST_H_
#define GEOM_TEST_H_

// The main function to run all graphics tests
int run_geom_tests(void);

// The individual graphics tests. Tests should be separated by component
int get_larger_y_test(void);
int get_smaller_y_test(void);
int inside_bounds_test(void);

#endif
